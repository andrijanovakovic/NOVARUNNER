var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
const { io } = require("./bin/www");

const keys = require("./keys/index");

var mongoose = require("mongoose");
mongoose.connect(keys.db.connection_string, { useNewUrlParser: true, useUnifiedTopology: true }).then((db_res) => {
  console.log(`${new Date()} - db connection successfull!`);
});

// import data models here
require("./db/models/User");
require("./db/models/UserInfo");
require("./db/models/Run");
require("./db/models/RunStatistic");
require("./db/models/RunRoom");

var app = express();

// import routers here
var user_router = require("./routes/user");
var user_info_router = require("./routes/user_info");
var run_router = require("./routes/run");
var run_statistic_router = require("./routes/run_statistic");
var run_room_router = require("./routes/run_room");

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// set routing here
app.use("/user", user_router);
app.use("/user-info", user_info_router);
app.use("/run", run_router);
app.use("/run-statistic", run_statistic_router);
app.use("/run-room", run_room_router);

app.use("*", (req, res) => {
  res.json({ success: false, message: "Ooops, prišlo je do napake. Stran, ki jo iščete, ne obstaja." }).status(404);
});

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.json({ success: false, message: "Prišlo je do nepričakovane napake v programu." }).status(500);
});

module.exports = app;
