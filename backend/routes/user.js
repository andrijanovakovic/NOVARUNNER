/**
 * reach routes in this file via "/user/*"
 */

var express = require("express");
var router = express.Router();
const { check, validationResult } = require("express-validator");
const cloneDeep = require("lodash/cloneDeep");
const mongoose = require("mongoose");
const User = mongoose.model("User");
const UserInfo = mongoose.model("UserInfo");
const bcrypt = require("bcrypt");
const keys = require("../keys/index");
const { generate_token, authenticate_token } = require("../helpers/jwt");

const { salt_rounds } = keys.bcrypt;

router.post(
  "/change-password",
  [check("password").notEmpty().withMessage("Geslo je obvezen podatek.").isLength({ min: 8 }).withMessage("Geslo mora biti dolzine vsaj 8 znakov.")],
  authenticate_token,
  async (req, res) => {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ success: false, message: "Neuspešno!", error: errors.array() }).status(400);
    }

    const { password } = req.body;
    const { _id } = req.token_decoded.user;

    try {
      const password_hashed = await bcrypt.hash(password, salt_rounds);
      const data = await User.updateOne({ _id }, { password: password_hashed });
      return res.json({ success: true, message: "Uspešno!", data }).status(200);
    } catch (error) {
      console.log(error);
      return res.json({ success: false, message: "Napaka!", error }).status(400);
    }
  },
);

router.post(
  "/register",
  [
    check("username")
      .notEmpty()
      .withMessage("Uporabnisko ime je obvezen podatek.")
      .isLength({ min: 6 })
      .withMessage("Uporabnisko ime mora biti dolzine vsaj 6 znakov")
      .custom(async (value) => {
        const user = await User.findOne({ username: value });
        if (user) {
          throw new Error("Uporabnisko ime zasedeno!");
        }
        return true;
      }),
    check("email")
      .notEmpty()
      .withMessage("E-naslov je obvezen podatek.")
      .isEmail()
      .withMessage("Nepravilen format e-naslova.")
      .custom(async (value) => {
        const user = await User.findOne({ email: value });
        if (user) {
          throw new Error("E-naslov zaseden!");
        }
        return true;
      }),
    check("password").notEmpty().withMessage("Geslo je obvezen podatek.").isLength({ min: 8 }).withMessage("Geslo mora biti dolzine vsaj 8 znakov."),
    check("password_repeat")
      .notEmpty()
      .withMessage("Ponovitev gesla je obvezen podatek.")
      .custom((value, { req }) => {
        if (value !== req.body.password) {
          throw new Error("Gesli se ne ujemata!");
        }
        return true;
      }),
  ],
  async (req, res) => {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ success: false, message: "Neuspesna registracija!", error: errors.array() }).status(400);
    }

    const { username, email, password } = req.body;

    // check if username or email is taken
    const _user = await User.findOne({ $or: [{ username: username }, { email: email }] });
    if (_user) {
      return res.json({ success: false, message: "Neuspesna registracija! Uporabnik ze obstaja." }).status(400);
    }

    const password_hashed = await bcrypt.hash(password, salt_rounds);

    const new_user = new User({ username, email, password: password_hashed });

    try {
      const r = await new_user.save();

      // create user info for the new user
      const new_user_info = new UserInfo({ user_id: r._id });
      const r1 = await new_user_info.save();

      // omit password from the data we are returning to user
      let _new_user = cloneDeep(new_user);
      delete _new_user._doc.password;

      return res.json({ success: true, message: "Uspesna registracija!", user: _new_user }).status(200);
    } catch (error) {
      console.log(error);
      return res.json({ success: false, message: "Napaka pri zapisovanju v podatkovno bazo!", error }).status(400);
    }
  },
);

router.post(
  "/login",
  [check("identifier").notEmpty().withMessage("Uporabnisko ime/e-naslov je obvezen podatek."), check("password").notEmpty().withMessage("Geslo je obvezen podatek.")],
  async (req, res) => {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ success: false, message: "Neuspesna prijava!", error: errors.array() }).status(400);
    }

    const { identifier, password } = req.body;

    const user = await User.findUserByIdentifier(identifier);
    if (!user) {
      return res.json({ success: false, message: "Uporabnik ne obstaja." }).status(400);
    }

    const password_correct = await bcrypt.compare(password, user.password);
    if (!password_correct) {
      return res.json({ success: false, message: "Neuspesna prijava, geslo nepravilno!" }).status(400);
    }

    let user_copy = cloneDeep(user._doc);
    delete user_copy.password;

    // generate jwt token here
    var token = generate_token({ user: user_copy });

    return res.json({ success: true, message: "Uspesna prijava!", user: user_copy, token }).status(200);
  },
);

router.get("/get-all", async (req, res) => {
  try {
    const users = await User.find().select(["-password"]);
    return res.json({ success: true, message: "Uspešno!", users }).status(200);
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

router.get("/get-test-user-by-identifier", async (req, res) => {
  const { identifier } = req.body;
  try {
    const data = await User.findTestUserByIdentifier(identifier).select(["-password"]);
    return res.json({ success: true, message: "Uspešno!", data: data }).status(200);
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

module.exports = router;
