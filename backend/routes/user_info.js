var express = require("express");
var router = express.Router();
const { check, validationResult } = require("express-validator");
const mongoose = require("mongoose");
const UserInfo = mongoose.model("UserInfo");
const User = mongoose.model("User");
const { authenticate_token } = require("../helpers/jwt");

/**
 * todo: get user_id from token if using jwt tokens...
 */
router.post("/create", [check("user_id").notEmpty().withMessage("Polje 'user_id' je obvezno!")], async (req, res) => {
  var errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.json({ success: false, message: "Neuspesno!", error: errors.array() }).status(400);
  }

  const { gender = null, height_cm = null, weight_kg = null, date_of_birth = null, name = null, surname = null, nickname = null, user_id } = req.body;

  // get user
  let user = null;
  try {
    user = await User.findById(user_id);
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }

  if (!user) {
    return res.json({ success: false, message: "Uporabnik ne obstaja!" }).status(400);
  }

  // get user info
  let user_info = null;
  try {
    user_info = await UserInfo.findOne({ user_id: user_id });
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }

  if (user_info) {
    // update object, set new values
    user_info.gender = gender;
    user_info.height_cm = height_cm;
    user_info.weight_kg = weight_kg;
    user_info.date_of_birth = date_of_birth;
    user_info.name = name;
    user_info.surname = surname;
    user_info.nickname = nickname;

    try {
      const r = await user_info.save();
      return res.json({ success: true, message: "Uspešno!", user_info });
    } catch (error) {
      console.log(error);
      return res.json({ success: false, message: "Prišlo je do napake!", error });
    }
  } else {
    // create new user info
    const new_user_info = new UserInfo({ gender, height_cm, weight_kg, date_of_birth, name, surname, nickname, user_id });
    try {
      const r = await new_user_info.save();
      return res.json({ success: true, message: "Uspešno!", user_info: new_user_info });
    } catch (error) {
      console.log(error);
      return res.json({ success: false, message: "Prišlo je do napake!", error });
    }
  }
});

router.get("/get", authenticate_token, async (req, res) => {
  const { _id } = req.token_decoded.user;

  // check if user info exists for given user_id
  try {
    let user_info = await UserInfo.findOne({ user_id: _id });

    if (!user_info) {
      return res.json({ success: false, message: "Ni podatkov!" }).status(400);
    }

    return res.json({ success: true, message: "Uspesno!", data: user_info }).status(200);
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

router.post("/save", authenticate_token, async (req, res) => {
  const { _id } = req.token_decoded.user;
  const { gender, height_cm, weight_kg, date_of_birth, name, surname, nickname } = req.body;

  // first find user info object
  let user_info = await UserInfo.findOne({ user_id: _id });
  if (!user_info) {
    return res.json({ success: false, message: "Ni podatkov!" }).status(400);
  }

  user_info.gender = gender;
  user_info.height_cm = height_cm;
  user_info.weight_kg = weight_kg;
  user_info.date_of_birth = date_of_birth;
  user_info.name = name;
  user_info.surname = surname;
  user_info.nickname = nickname;

  // check if user info exists for given user_id
  try {
    let r = await user_info.save();
    return res.json({ success: true, message: "Uspesno!", data: user_info }).status(200);
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

module.exports = router;
