/**
 * reach routes in this file via "/run/*"
 */

var express = require("express");
var router = express.Router();
const { check, validationResult } = require("express-validator");
const mongoose = require("mongoose");
const { authenticate_token } = require("../helpers/jwt");
const Run = mongoose.model("Run");

/**
 * todo: get user_id from token if using jwt tokens...
 */
router.post("/start", authenticate_token, async (req, res) => {
  const { _id } = req.token_decoded.user;

  try {
    let unfinished_run = await Run.getUnfinishedRunByUserId(_id);
    if (unfinished_run) {
      return res.json({ success: false, message: "Obstaja nedokončan trening!", data: unfinished_run }).status(400);
    }
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }

  const {
    target_duration_in_seconds = null,
    target_distance_in_meters = null,
    datetime_started = new Date(),
    datetime_ended = null,
    overall_distance_in_meters = null,
    overall_elevation_change_in_meters = null,
    overall_average_speed = null,
  } = req.body;

  let new_run = new Run({
    target_duration_in_seconds,
    target_distance_in_meters,
    datetime_started,
    datetime_ended,
    overall_distance_in_meters,
    overall_elevation_change_in_meters,
    overall_average_speed,
    user_id: _id,
  });

  try {
    let r = await new_run.save();
    return res.json({ success: true, message: "Uspešno!", run: new_run }).status(200);
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

router.post("/stop", authenticate_token, async (req, res) => {
  const {
    current_run_time_elapsed_in_seconds,
    current_run_time_elapsed_formatted,
    current_run_distance_in_meters,

    current_run_max_speed_in_kmh,
    current_run_pace_in_minutes_per_km,
    current_run_average_speed_in_kmh,
    current_run_polyline_coordinates,

    current_run_min_altitude_in_meters,
    current_run_max_altitude_in_meters,
    current_run_overall_altitude_change_in_meters,
    current_run_duration_goal_achieved,
    current_run_distance_goal_achieved,
    run_id,
    datetime_ended = new Date(),
  } = req.body;

  const { _id } = req.token_decoded.user;

  // get run
  let last_run = null;
  try {
    last_run = await Run.findOne({ $and: [{ user_id: _id }, { _id: run_id }, { datetime_ended: null }] });
    if (!last_run) {
      return res.json({ success: false, message: "Neuspesno! Ni podatkov." }).status(400);
    }
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }

  last_run.duration_in_seconds = current_run_time_elapsed_in_seconds;
  last_run.duration_formatted_as_hh_mm_ss = current_run_time_elapsed_formatted;
  last_run.duration_goal_achieved = current_run_duration_goal_achieved;
  last_run.distance_goal_achieved = current_run_distance_goal_achieved;
  last_run.distance_in_meters = current_run_distance_in_meters;
  last_run.max_speed_in_kmh = current_run_max_speed_in_kmh;
  last_run.average_speed_in_kmh = current_run_average_speed_in_kmh;
  last_run.pace_in_minutes_per_km = current_run_pace_in_minutes_per_km;
  last_run.min_altitude_in_meters = current_run_min_altitude_in_meters;
  last_run.max_altitude_in_meters = current_run_max_altitude_in_meters;
  last_run.overall_altitude_change_in_meters = current_run_overall_altitude_change_in_meters;
  last_run.datetime_ended = datetime_ended;
  last_run.polyline_coordinates = current_run_polyline_coordinates;

  try {
    const r = last_run.save();
    return res.json({ success: true, message: "Uspešno.", data: last_run }).status(200);
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

router.get("/all", authenticate_token, async (req, res) => {
  var errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.json({ success: false, message: "Neuspešno!", error: errors.array() }).status(400);
  }

  const { _id } = req.token_decoded.user;

  try {
    const user_runs = await Run.find({ user_id: _id }).sort({ datetime_started: "desc" });
    return res.json({ success: true, message: "Uspešno.", runs: user_runs });
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

router.get("/last", authenticate_token, async (req, res) => {
  const { _id } = req.token_decoded.user;

  try {
    const last_run = await Run.findOne({ user_id: _id, datetime_ended: { $ne: null } }).sort({ datetime_started: "desc" });
    return res.json({ success: true, message: "Uspešno.", last_run });
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

router.get("/unfinished", authenticate_token, async (req, res) => {
  const { _id } = req.token_decoded.user;

  try {
    const data = await Run.findOne({ user_id: _id, datetime_started: { $ne: null }, datetime_ended: null }).sort({ datetime_started: "desc" });
    return res.json({ success: true, message: "Uspešno.", data });
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

router.post("/clear-user-trainings", authenticate_token, async (req, res) => {
  const { _id } = req.token_decoded.user;

  try {
    let runs = await Run.find({ user_id: _id, datetime_started: { $ne: null }, datetime_ended: null });

    await runs.map(async (run) => {
      run.datetime_ended = new Date();
      await run.save();
    });

    return res.json({ success: true, message: "Uspešno." });
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

router.get("/previous-runs", authenticate_token, async (req, res) => {
  const { _id } = req.token_decoded.user;

  try {
    const data = await Run.find({ user_id: _id, datetime_started: { $ne: null }, datetime_ended: { $ne: null } }).sort({ datetime_started: "desc" });

    let _total_run_meters = 0;
    let _total_run_time_in_seconds = 0;
    let _average_speed_in_kmh = 0;
    let _average_speed_in_kmh_number_of_items = 0;
    let _average_pace_in_minutes_per_km = 0;
    let _average_pace_in_minutes_per_km_number_of_items = 0;
    let _max_speed_in_kmh = 0;
    let _fastest_pace_in_minutes_per_km = Number.POSITIVE_INFINITY;

    for (let i = 0, leni = data.length; i < leni; i++) {
      const { distance_in_meters = null, duration_in_seconds = null, average_speed_in_kmh = null, max_speed_in_kmh = null, pace_in_minutes_per_km = null } = data[i]._doc;
      if (distance_in_meters) _total_run_meters += distance_in_meters;
      if (duration_in_seconds) _total_run_time_in_seconds += duration_in_seconds;
      if (average_speed_in_kmh) {
        _average_speed_in_kmh += average_speed_in_kmh;
        _average_speed_in_kmh_number_of_items += 1;
      }
      if (pace_in_minutes_per_km) {
        _average_pace_in_minutes_per_km += pace_in_minutes_per_km;
        _average_pace_in_minutes_per_km_number_of_items += 1;
        if (pace_in_minutes_per_km < _fastest_pace_in_minutes_per_km) {
          _fastest_pace_in_minutes_per_km = pace_in_minutes_per_km;
        }
      }
      if (max_speed_in_kmh > _max_speed_in_kmh) _max_speed_in_kmh = max_speed_in_kmh;
    }

    _average_speed_in_kmh /= _average_speed_in_kmh_number_of_items;
    _average_pace_in_minutes_per_km /= _average_pace_in_minutes_per_km_number_of_items;

    // fix numbers
    _total_run_meters = Number(_total_run_meters.toFixed(2));
    _average_speed_in_kmh = Number(_average_speed_in_kmh.toFixed(2));
    _average_pace_in_minutes_per_km = Number(_average_pace_in_minutes_per_km.toFixed(2));

    return res.json({
      success: true,
      message: "Uspešno.",
      data,
      stats: {
        total_run_meters: _total_run_meters,
        total_run_time_in_seconds: _total_run_time_in_seconds,
        average_speed_in_kmh: _average_speed_in_kmh,
        average_pace_in_minutes_per_km: _average_pace_in_minutes_per_km,
        max_speed_in_kmh: _max_speed_in_kmh,
        fastest_pace_in_minutes_per_km: _fastest_pace_in_minutes_per_km,
      },
    });
  } catch (error) {
    console.log(error);
    return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
  }
});

module.exports = router;
