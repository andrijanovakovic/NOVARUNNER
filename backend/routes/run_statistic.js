var express = require("express");
var router = express.Router();
const { check, validationResult } = require("express-validator");
const mongoose = require("mongoose");
const Run = mongoose.model("Run");
const RunStatistic = mongoose.model("RunStatistic");

/**
 * todo: get user_id from token if using jwt tokens...
 */
router.post(
	"/create",
	[check("user_id").notEmpty().withMessage("Polje 'user_id' je obvezno!"), check("run_id").notEmpty().withMessage("Polje 'run_id' je obvezno!")],
	async (req, res) => {
		var errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.json({ success: false, message: "Neuspesno!", error: errors.array() }).status(400);
		}

		// get data from body
		const { user_id, run_id } = req.body;

		// find run
		let run = null;
		try {
			run = await Run.findOne({ $and: [{ _id: run_id }, { user_id: user_id }, { datetime_ended: null }] });
			if (!run) {
				return res.json({ success: false, message: "Ni podatkov!" }).status(400);
			}
		} catch (error) {
			console.log(error);
			return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
		}

		const { lattitude, longitude, speed_kph, elevation_in_meters } = req.body;

		const rs = new RunStatistic({ lattitude, longitude, speed_kph, elevation_in_meters, run_id });

		try {
			const r = await rs.save();
			return res.json({ success: true, message: "Uspešno", run_statistic: rs }).status(200);
		} catch (error) {
			console.log(error);
			return res.json({ success: false, message: "Prišlo je do napake!", error }).status(400);
		}
	},
);

module.exports = router;
