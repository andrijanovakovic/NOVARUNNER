/**
 * reach routes defined here by /run-room endpoint
 */

var express = require("express");
var router = express.Router();
const { check, validationResult } = require("express-validator");
const mongoose = require("mongoose");
const User = mongoose.model("User");
const RunRoom = mongoose.model("RunRoom");
const { authenticate_token } = require("../helpers/jwt");

router.get("/room-details", authenticate_token, [check("room_id").notEmpty().withMessage("ID sobe je obvezen podatek (room_id).")], async (req, res) => {
  const { room_id } = req.query;
  const room = await RunRoom.findById(room_id);

  if (!room) {
    return res.json({ success: false, message: "Soba ne obstaja." }).status(200);
  }

  const admin = await User.findById(room.admin_user_id).select(["-password"]);
  const guests = await User.find({ _id: { $in: room.guest_users_ids } }).select(["-password"]);

  return res.json({ success: true, message: "Uspesno!", data: { room, admin, guests } }).status(200);
});

router.get("/active-rooms", authenticate_token, async (req, res) => {
  const { _id } = req.token_decoded.user;

  /**
   * privatno sobo lahko vidijo samo uporabniki
   * ki so sobo ustvarili ali pa so med gosti sobe
   *
   * javno sobo lahko vidijo vsi
   */

  const public_rooms = await RunRoom.find({ room_is_private: false, datetime_deactivated: null });
  const private_rooms = await RunRoom.find({ room_is_private: true, datetime_deactivated: null, $or: [{ admin_user_id: _id }, { guest_users_ids: _id }] });

  return res.json({ success: true, message: "Uspesno!", data: [...public_rooms, ...private_rooms] }).status(200);
});

router.post(
  "/create",
  authenticate_token,
  [
    check("guest_users_identifiers").isArray().notEmpty().withMessage("Polje 'guest_users_identifiers' je obvezno!"),
    check("room_name").notEmpty().withMessage("Polje 'room_name' je obvezno!"),
    check("room_is_private").isBoolean().notEmpty().withMessage("Polje 'room_is_private' je obvezno!"),
  ],
  async (req, res) => {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ success: false, message: "Neuspesno!", error: errors.array() }).status(400);
    }

    const { _id } = req.token_decoded.user;

    const { guest_users_identifiers, room_name, datetime_run_start = new Date(), target_duration_in_seconds = null, target_distance_in_meters = null, room_is_private } = req.body;

    // find guests
    let guest_users = await User.find({
      $or: [{ username: { $in: guest_users_identifiers } }, { email: { $in: guest_users_identifiers } }],
    });

    let guest_users_ids = guest_users.map((u) => u._doc._id);

    const new_room = new RunRoom({
      admin_user_id: _id,
      guest_users_ids,
      name: room_name,
      datetime_run_start,
      target_duration_in_seconds,
      target_distance_in_meters,
      room_is_private,
    });

    try {
      const r = await new_room.save();
      return res.json({ success: true, message: "Uspesno!", data: new_room }).status(200);
    } catch (error) {
      console.log(error);
      return res.json({ success: false, message: "Prislo je do napake!", error }).status(400);
    }
  },
);

router.post("/deactivate", authenticate_token, [check("room_id").notEmpty().withMessage("Polje 'room_id' je obvezno!")], async (req, res) => {
  var errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.json({ success: false, message: "Neuspesno!", error: errors.array() }).status(400);
  }

  const { _id } = req.token_decoded.user;
  const { room_id } = req.body;

  let room = await RunRoom.findOne({ _id: room_id }, { active: true }, { admin_user_id: _id });

  if (!room) {
    return res.json({ success: false, message: "Soba ne obstaja!" }).status(400);
  }
  
  room.active = false;
  room.datetime_deactivated = new Date();
  await room.save();
  return res.json({ success: true, message: "Uspešno!", data: room });
});

module.exports = router;
