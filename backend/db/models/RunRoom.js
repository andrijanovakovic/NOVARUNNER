const mongoose = require("mongoose");
const { Schema } = mongoose;

const run_room_schema = new Schema(
  {
    active: { type: Boolean, required: false, default: true },
    admin_user_id: { type: Schema.Types.ObjectId, required: true },
    guest_users_ids: { type: [Schema.Types.ObjectId], required: true },
    name: { type: String, required: true },
    datetime_run_start: { type: Date, required: true },
    target_duration_in_seconds: { type: Number },
    target_distance_in_meters: { type: Number },
    datetime_deactivated: { type: Date, required: false },
    room_is_private: { type: Boolean, required: false, default: true },
  },
  { timestamps: true },
);

mongoose.model("RunRoom", run_room_schema);
