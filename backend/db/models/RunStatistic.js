const mongoose = require("mongoose");
const { Schema } = mongoose;

const run_statistic_schema = new Schema(
	{
		lattitude: { type: String },
		longitude: { type: String },
		speed_kph: { type: Number },
		elevation_in_meters: { type: Number },
		run_id: { type: Schema.Types.ObjectId, ref: "Run", required: true },
	},
	{ timestamps: true },
);

mongoose.model("RunStatistic", run_statistic_schema);
