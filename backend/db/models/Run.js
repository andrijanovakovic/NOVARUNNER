const mongoose = require("mongoose");
const { Schema } = mongoose;

const run_schema = new Schema(
  {
    // duration and duration goals
    duration_in_seconds: { type: Number },
    duration_formatted_as_hh_mm_ss: { type: String },
    target_duration_in_seconds: { type: Number },
    duration_goal_achieved: { type: Boolean },

    // distance and distance goals
    target_distance_in_meters: { type: Number },
    distance_goal_achieved: { type: Boolean },
    distance_in_meters: { type: Number },

    // speed
    max_speed_in_kmh: { type: Number },
    average_speed_in_kmh: { type: Number },
    pace_in_minutes_per_km: { type: Number },

    // altitudes
    min_altitude_in_meters: { type: Number },
    overall_altitude_change_in_meters: { type: Number },
    max_altitude_in_meters: { type: Number },

    // dates
    datetime_started: { type: Date, required: true },
    datetime_ended: { type: Date, required: false },

    // user responsible
    user_id: { type: Schema.Types.ObjectId, ref: "User", required: true },

    // route
    polyline_coordinates: {
      type: [
        {
          latitude: Number,
          longitude: Number,
        },
      ],
    },
  },
  { timestamps: true },
);

run_schema.statics.getUnfinishedRunByUserId = function (user_id, cb = null) {
  return this.findOne({ datetime_started: { $ne: null }, datetime_ended: null, user_id }, cb);
};

mongoose.model("Run", run_schema);
