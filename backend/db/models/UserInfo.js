const mongoose = require("mongoose");
const { Schema } = mongoose;

const user_info_schema = new Schema(
  {
    gender: { type: String, enum: ["M", "F", "O"] },
    height_cm: { type: Number },
    weight_kg: { type: Number },
    date_of_birth: { type: String },
    name: { type: String },
    surname: { type: String },
    nickname: { type: String },
    user_id: { type: Schema.Types.ObjectId, ref: "User", required: true },
  },
  { timestamps: true },
);

// name should look like John Johnny Smith
user_info_schema.methods.setFullNameWithNickname = function (_name = "") {
  const data = _name.split[" "];
  this.name = data[0];
  this.nickname = data[1];
  this.surname = data[2];
  this.save();
};

user_info_schema.methods.getFullName = function () {
  return this.name + this.surname;
};

mongoose.model("UserInfo", user_info_schema);
