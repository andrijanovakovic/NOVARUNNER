const mongoose = require("mongoose");
const { Schema } = mongoose;

const user_schema = new Schema(
  {
    username: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    is_test: { type: Boolean, required: false },
  },
  { timestamps: true },
);

user_schema.statics.getAllTestUsers = function (identifier, cb) {
  return this.find({ is_test: true }, cb);
};

user_schema.statics.findTestUserByIdentifier = function (identifier, cb) {
  return this.findOne({ $or: [{ username: identifier }, { email: identifier }], is_test: true }, cb);
};

user_schema.statics.findUserByIdentifier = function (identifier, cb) {
  return this.findOne({ $or: [{ username: identifier }, { email: identifier }] }, cb);
};

mongoose.model("User", user_schema);

/**
 * Primeri uporabe
 *
 * const User = mongoose.model("User");
 *
 * const some_user = await User.findOne({ $or: [{ username: identifier }, { email: identifier }] });
 * user.password = "<new_password_hashed">
 */
