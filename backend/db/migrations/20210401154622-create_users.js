const keys = require("../../keys/index");
const bcrypt = require("bcrypt");

module.exports = {
	async up(db, client) {
		await db.collection("users").insertOne({
			username: "andrija",
			email: "andrija.novakovic@student.um.si",
			password: await bcrypt.hash("andrija123", keys.bcrypt.salt_rounds),
			createdAt: new Date(),
			updatedAt: new Date(),
		});
	},

	async down(db, client) {
		await db.collection("users").deleteOne({ username: "andrija" });
	},
};
