const jwt = require("jsonwebtoken");
const keys = require("../keys");

const generate_token = (data_for_serializing) => {
  return jwt.sign({ ...data_for_serializing }, keys.jwt.secret);
};

const authenticate_token = (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null) return res.sendStatus(401);

  jwt.verify(token, keys.jwt.secret, (err, token_decoded) => {
    if (err) {
      console.log(err);
      return res.sendStatus(403);
    }
    req.token_decoded = token_decoded;
    next();
  });
};

module.exports = {
  generate_token,
  authenticate_token,
};
