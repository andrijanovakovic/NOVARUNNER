const io = require("socket.io")();

const wsapi = {
  io: io,
};

io.on("connection", function (socket) {
  socket.on("join_room", (data) => {
    const { room_id, user_id } = data;
    socket.join(room_id);
  });

  socket.on("leave_room", (data) => {
    const { room_id, user_id } = data;
    socket.leave(room_id);
  });

  socket.on("update_run_room_stats", (data) => {
    const { room_id, user, stats } = data;
    io.to(room_id).emit("update_run_room_stats", { user, stats });
  });

  socket.on("stop_run_in_room", (data) => {
    const { room_id } = data;
    io.to(room_id).emit("stop_run_in_room", {});
  });

  socket.on("disconnect", (data) => {
    console.log(data);
  });
});

module.exports = wsapi;
