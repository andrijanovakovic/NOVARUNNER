const request = require("supertest");
const app = require("../app");

/**
 *
 */
describe("start training test", () => {
  let token = "";
  beforeAll(async () => {
    const r = await request(app).post("/user/login").send({
      identifier: "andrijanovakovic",
      password: "test_password",
    });

    token = `Bearer ${r.body.token}`;

    await request(app).post("/run/clear-user-trainings").set("Authorization", token).send({});
  });

  let run_id = "";

  it("starts user training", async () => {
    const r = await request(app).post("/run/start").set("Authorization", token).send({});
    expect(r.body).toHaveProperty("success");
    expect(r.body.success).toBe(true);
    run_id = r.body.run._id;
  });

  it("SHOULD NOT start user training because a training is already started", async () => {
    const r = await request(app).post("/run/start").set("Authorization", token).send({});
    expect(r.body).toHaveProperty("success");
    expect(r.body.success).toBe(false);
  });

  it("should stop user training", async () => {
    const r = await request(app).post("/run/stop").set("Authorization", token).send({
      current_run_time_elapsed_in_seconds: 100,
      current_run_time_elapsed_formatted: "00:01:40",
      current_run_distance_in_meters: 222,

      current_run_max_speed_in_kmh: 12.3,
      current_run_pace_in_minutes_per_km: 5.0,
      current_run_average_speed_in_kmh: 4.75,
      current_run_polyline_coordinates: [],

      current_run_min_altitude_in_meters: 0,
      current_run_max_altitude_in_meters: 0,
      current_run_overall_altitude_change_in_meters: 0,
      current_run_duration_goal_achieved: false,
      current_run_distance_goal_achieved: false,
      run_id: run_id,
    });
    expect(r.body).toHaveProperty("success");
    expect(r.body.success).toBe(true);
  });

  it("should NOT stop user training", async () => {
    const r = await request(app).post("/run/stop").set("Authorization", token).send({
      current_run_time_elapsed_in_seconds: 100,
      current_run_time_elapsed_formatted: "00:01:40",
      current_run_distance_in_meters: 222,

      current_run_max_speed_in_kmh: 12.3,
      current_run_pace_in_minutes_per_km: 5.0,
      current_run_average_speed_in_kmh: 4.75,
      current_run_polyline_coordinates: [],

      current_run_min_altitude_in_meters: 0,
      current_run_max_altitude_in_meters: 0,
      current_run_overall_altitude_change_in_meters: 0,
      current_run_duration_goal_achieved: false,
      current_run_distance_goal_achieved: false,
      run_id: run_id,
    });
    expect(r.body).toHaveProperty("success");
    expect(r.body.success).toBe(false);
  });
});
