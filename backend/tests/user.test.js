const request = require("supertest");
const app = require("../app");

/**
 * testiranje registracije uporabnika
 */
describe("post /user/register", () => {
  it("should register a new user", async () => {
    const res = await request(app).post("/user/register").send({
      username: "testni_uporabnik_username",
      password: "testni_uporabnik_password",
      password_repeat: "testni_uporabnik_password",
      email: "testniemail@student.um.si",
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(true);
  });
});

/**
 * fail registracija
 */
describe("post /user/register", () => {
  it("should fail to register a new user because email is already taken", async () => {
    const res = await request(app).post("/user/register").send({
      username: "testni_uporabnik_username",
      password: "testni_uporabnik_password",
      password_repeat: "testni_uporabnik_password",
      email: "testniemail@student.um.si",
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(false);
  });
});

/**
 * fail registracija
 */
describe("post /user/register", () => {
  it("should fail to register a new user because username is already taken", async () => {
    const res = await request(app).post("/user/register").send({
      username: "testni_uporabnik_username",
      password: "testni_uporabnik_password",
      password_repeat: "testni_uporabnik_password",
      email: "testniemail@student.um.si",
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(false);
  });
});

describe("post /user/register", () => {
  it("should fail to register a new user because field 'username' is missing", async () => {
    const res = await request(app).post("/user/register").send({
      password: "testni_uporabnik_password",
      password_repeat: "testni_uporabnik_password",
      email: "testni_uporabnik_email",
    });
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(false);
  });
});

describe("post /user/register", () => {
  it("should fail to register a new user because fields 'password' and 'password_repeat' do not have same value", async () => {
    const res = await request(app).post("/user/register").send({
      username: "testni_uporabnik_username",
      password: "testni_uporabnik_password",
      password_repeat: "different value",
      email: "testni_uporabnik_email",
    });
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(false);
  });
});

describe("post /user/login", () => {
  it("should login user successfully via identifier = 'username'", async () => {
    const res = await request(app).post("/user/login").send({
      identifier: "testni_uporabnik_username",
      password: "testni_uporabnik_password",
    });
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(true);
    expect(res.body).toHaveProperty("message");
    expect(res.body.message).toBe("Uspesna prijava!");
  });
});

describe("post /user/login", () => {
  it("should login user successfully via identifier = 'email'", async () => {
    const res = await request(app).post("/user/login").send({
      identifier: "testniemail@student.um.si",
      password: "testni_uporabnik_password",
    });
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(true);
    expect(res.body).toHaveProperty("message");
    expect(res.body.message).toBe("Uspesna prijava!");
  });
});

describe("post /user/login", () => {
  it("should fail to login user because field 'identifier' is missing", async () => {
    const res = await request(app).post("/user/login").send({
      password: "testni_uporabnik_password",
    });
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(false);
  });
});

describe("post /user/login", () => {
  it("should fail to login user because field 'password' is missing", async () => {
    const res = await request(app).post("/user/login").send({
      identifier: "testniemail@student.um.si",
    });
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(false);
  });
});

describe("post /user/login", () => {
  it("should fail to login user because no body is given", async () => {
    const res = await request(app).post("/user/login").send({
      identifier: "testniemail@student.um.si",
    });
    expect(res.body).toHaveProperty("success");
    expect(res.body.success).toBe(false);
  });
});

describe("post /user/change-password", () => {
  it("should change user password successfully", async () => {
    const r1 = await request(app).post("/user/login").send({
      identifier: "andrijanovakovic",
      password: "new_password",
    });

    let token = `Bearer ${r1.body.token}`;

    const r2 = await request(app).post("/user/change-password").set("Authorization", token).send({
      password: "new_password1",
    });

    const r3 = await request(app).post("/user/login").send({
      identifier: "andrijanovakovic",
      password: "new_password1",
    });

    expect(r3.body).toHaveProperty("success");
    expect(r3.body.success).toBe(true);
  });
});
