const request = require("supertest");
const app = require("../app");
const isEqual = require("lodash/isEqual");

/**
 *
 */
describe("post /user-info/save", () => {
  it("prevents gender to be anything other that M|F", async () => {
    const r = await request(app).post("/user/login").send({
      identifier: "andrijanovakovic",
      password: "test_password",
    });

    let token = `Bearer ${r.body.token}`;

    const r1 = await request(app).post("/user-info/save").set("Authorization", token).send({
      gender: "A",
    });

    expect(r1.body).toHaveProperty("success");
    expect(r1.body.success).toBe(false);
  });
});

/**
 *
 */
describe("test user info", () => {
  let token = "";
  let user_info1 = null;
  let user_info2 = null;

  beforeAll(async () => {
    const r = await request(app).post("/user/login").send({
      identifier: "andrijanovakovic",
      password: "test_password",
    });

    token = `Bearer ${r.body.token}`;
  });

  it("fetches user info by id from token", async () => {
    const r = await request(app).get("/user-info/get").set("Authorization", token).send({});
    expect(r.body).toHaveProperty("success");
    expect(r.body.success).toBe(true);
    user_info1 = r.body.data;
  });

  it("updates user info by id from token", async () => {
    const r = await request(app)
      .post("/user-info/save")
      .set("Authorization", token)
      .send({
        name: Math.random().toString(36).substring(7),
      });
    expect(r.body).toHaveProperty("success");
    expect(r.body.success).toBe(true);
    user_info2 = r.body.data;
  });

  it("checks if user info has been changed successfully", async () => {
    const r = isEqual(user_info1, user_info2);
    expect(r).toBe(false);
  });
});
