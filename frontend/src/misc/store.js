import { createStore, applyMiddleware } from "redux";

// store middleware
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

import reducers from "../reducers/";

// push redux middlewares to this array
let middlewares_to_be_applied = [];

// add thunk
middlewares_to_be_applied.push(thunk);

// create middleware
const middleware = applyMiddleware(...middlewares_to_be_applied);

// create store
const store = createStore(reducers, composeWithDevTools(middleware));

// export store
export default store;
