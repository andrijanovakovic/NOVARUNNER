import AsyncStorage from "@react-native-async-storage/async-storage";

export const nr_as_get_string = async (key) => {
  try {
    return await AsyncStorage.getItem(key);
  } catch (error) {
    console.log("NOVARUNNER ASYNCSTORAGE ERROR:", error);
    return null;
  }
};

export const nr_as_set_string = async (key, value) => {
  try {
    return await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.log("NOVARUNNER ASYNCSTORAGE ERROR:", error);
    return null;
  }
};

export const nr_as_get_object = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (error) {
    console.log("NOVARUNNER ASYNCSTORAGE ERROR:", error);
    return null;
  }
};

export const nr_as_set_object = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    return await AsyncStorage.setItem(key, jsonValue);
  } catch (error) {
    console.log("NOVARUNNER ASYNCSTORAGE ERROR:", error);
    return null;
  }
};
