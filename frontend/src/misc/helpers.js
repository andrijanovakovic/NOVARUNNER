import { Platform, StatusBar, Alert } from "react-native";
import { API_URL, OUTPUT_NETWORK_REQUESTS } from "@env";
import axios from "axios";
import SimpleToast from "react-native-simple-toast";
import { v4 as generate_v4_uuid } from "uuid";
import { nr_as_get_string } from "./async_storage";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const get_uuid = () => {
  return generate_v4_uuid();
};

export const nr_message_box = async (title = "", message = "", answers = [{ text: "", should_resolve: false }]) =>
  new Promise((resolve) => {
    Alert.alert(
      title,
      message,
      answers.map((answer) => ({
        text: answer.text,
        onPress: () => resolve(answer.should_resolve),
      })),
      { cancelable: false },
    );
  });

export const register_network_requests = () => {
  if (process.env.NODE_ENV == "development" && !!OUTPUT_NETWORK_REQUESTS) {
    global.XMLHttpRequest = global.originalXMLHttpRequest ? global.originalXMLHttpRequest : global.XMLHttpRequest;
    global.FormData = global.originalFormData ? global.originalFormData : global.FormData;

    fetch; // Ensure to get the lazy property

    if (window.__FETCH_SUPPORT__) {
      // it's RNDebugger only to have
      window.__FETCH_SUPPORT__.blob = false;
    } else {
      /*
       * Set __FETCH_SUPPORT__ to false is just work for `fetch`.
       * If you're using another way you can just use the native Blob and remove the `else` statement
       */
      global.Blob = global.originalBlob ? global.originalBlob : global.Blob;
      global.FileReader = global.originalFileReader ? global.originalFileReader : global.FileReader;
    }
  }
};

/**
 *
 * @param {Object} obj
 * @returns true if obj = {}, false if obj = { ...keys and values... };
 */
export const object_is_empty = (obj = {}) => {
  return !obj || Object.keys(obj).length == 0;
};

export const configure_axios = async () => {
  const apiUrl = await AsyncStorage.getItem("NOVARUNNER#apiUrl") || API_URL;
  
  // set base url
  axios.defaults.baseURL = apiUrl;
  // set headers
  axios.defaults.headers.post["Content-Type"] = "application/json";

  // request timeout
  axios.defaults.timeout = process.env.NODE_ENV === "development" ? 10000000 : 10000;

  // request interceptor to add token to request headers
  axios.interceptors.request.use(
    async (config) => {
      const token = await nr_as_get_string("jwt_token");

      if (token) {
        config.headers = {
          authorization: `Bearer ${token}`,
        };
      }

      return config;
    },
    (error) => Promise.reject(error),
  );

  // response interceptor intercepting 401 responses, refreshing token and retrying the request
  axios.interceptors.response.use(
    (response) => response,
    async (error) => {
      console.log(error);

      const config = error.config;

      if (error?.response?.status === 401 && !config._retry) {
        config._retry = true;

        // todo: refresh access token here
        // localStorage.setItem("token", await refreshAccessToken());

        return axios(config);
      }

      return Promise.reject(error);
    },
  );
};

export const show_error_from_request_to_user = (r) => {
  if (!r || Object.keys(r).length == 0) {
    return;
  }

  if (r.data && r.data.error && r.data.error.length > 0) {
    let msg = r.data.error[0].msg;
    for (let i = 1, leni = r.data.error.length; i < leni; i++) {
      msg += `\n${r.data.error[i].msg}`;
    }
    SimpleToast.show(msg);
  } else if (r.data.message) {
    SimpleToast.show(r.data.message);
  } else if (r.message) {
    SimpleToast.show(r.data.message);
  }
};

export const configure_status_bar = () => {
  if (Platform.OS === "android") {
    StatusBar.setHidden(false);
    StatusBar.setBackgroundColor("rgba(0,0,0,0.2)", true);
    StatusBar.setTranslucent(true);
  }
};
