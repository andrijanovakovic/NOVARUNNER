// navigation
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { DarkTheme, NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React, { useEffect } from "react";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";

// icons
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";

// redux
import { Provider, useSelector } from "react-redux";
import store from "./misc/store";

// other
import { configure_axios, configure_status_bar, register_network_requests } from "./misc/helpers";
import "moment-duration-format";

// screens
import Welcome from "./screens/Welcome";
import Home from "./screens/Home";
import RunRooms from "./screens/RunRooms";
import RunRoomEdit from "./screens/RunRoomEdit";
import Login from "./screens/Login";
import PreviousRuns from "./screens/PreviousRuns";
import Register from "./screens/Register";
import RunDetails from "./screens/RunDetails";
import EditUserInfo from "./screens/EditUserInfo";
import Settings from "./screens/Settings";
import RunRoomDetails from "./screens/RunRoomDetails";
import { TouchableOpacity } from "react-native";

// const _SettingsLazy = React.lazy(() => import('./screens/Settings'));

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const header_titles = {
  PreviousRuns: "Treningi",
  Settings: "Nastavitve",
  RunRooms: "Run Rooms",
  Home: "Domov",
  BottomTabs: "Domov",
};
const get_header_title = (route) => {
  const route_name = getFocusedRouteNameFromRoute(route) ?? "BottomTabs";
  return route_name in header_titles ? header_titles[route_name] : "NOVARUNNER";
};

const get_header_right = (navigation) => {
  const route_name = getFocusedRouteNameFromRoute(navigation.route) ?? "BottomTabs";
  if (route_name == "RunRooms") {
    return () => (
      <TouchableOpacity
        onPress={() =>
          navigation.navigation.navigate("RunRoomEdit", {
            component_params: {
              action: "create",
            },
          })
        }
        style={{ marginRight: 7 }}
      >
        <MaterialIcon name={"add"} color={"#fff"} size={28} />
      </TouchableOpacity>
    );
  }
  return null;
};

const navigator_options = {
  cardStyle: { backgroundColor: "#000" },
  cardStyleInterpolator: ({ current: { progress } }) => ({
    cardStyle: {
      opacity: progress.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
      }),
    },
    overlayStyle: {
      opacity: progress.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 0.5],
        extrapolate: "clamp",
      }),
    },
  }),
};

function BottomTabsNavigator() {
  return (
    <Tab.Navigator lazy={true}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={(navigation) => ({
          tabBarLabel: "Domov",
          tabBarIcon: ({ color, size }) => <MaterialCommunityIcon name={"run"} color={color} size={size} />,
        })}
      />
      <Tab.Screen
        name="RunRooms"
        component={RunRooms}
        options={(navigation) => ({
          tabBarLabel: "Run Rooms",
          tabBarIcon: ({ color, size, focused }) => <MaterialCommunityIcon name={"run-fast"} color={focused ? "#E71D36" : color} size={size} />,
        })}
      />
      <Tab.Screen
        name="PreviousRuns"
        component={PreviousRuns}
        options={(navigation) => ({
          tabBarLabel: "Treningi",
          tabBarIcon: ({ color, size }) => <MaterialCommunityIcon name={"history"} color={color} size={size} />,
        })}
      />
      <Tab.Screen
        name="Settings"
        component={Settings}
        options={(navigation) => ({
          tabBarLabel: "Nastavitve",
          tabBarIcon: ({ color, size }) => <MaterialIcon name={"settings"} color={color} size={size} />,
        })}
        navigator_options={() => ({
          title: "Nastavitve",
        })}
      />
    </Tab.Navigator>
  );
}

function AppNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="BottomTabs"
        component={BottomTabsNavigator}
        options={(navigation) => ({ headerTitle: get_header_title(navigation.route), headerRight: get_header_right(navigation) })}
      />
      <Stack.Screen
        name="RunRoomEdit"
        component={RunRoomEdit}
        options={(navigation) => ({
          title: "Urejanje sobe",
        })}
      />
      <Stack.Screen
        name="RunRoomDetails"
        component={RunRoomDetails}
        options={(navigation) => ({
          title: "Soba",
        })}
      />
      <Stack.Screen
        name="RunDetails"
        component={RunDetails}
        options={(navigation) => ({
          title: "Podrobnosti",
        })}
      />
      <Stack.Screen
        name="EditUserInfo"
        component={EditUserInfo}
        options={(navigation) => ({
          title: "Urejanje",
        })}
      />
    </Stack.Navigator>
  );
}

function AuthNavigator() {
  return (
    <Stack.Navigator initialRouteName={"Welcome"} screenOptions={navigator_options} mode={"modal"}>
      <Stack.Screen name={"Welcome"} component={Welcome} options={{ title: "NOVARUNNER" }} />
      <Stack.Screen name={"Login"} component={Login} options={{ title: "Prijava" }} />
      <Stack.Screen name={"Register"} component={Register} options={{ title: "Registracija" }} />
    </Stack.Navigator>
  );
}

const AppContainer = (props) => {
  useEffect(() => {
    configure_axios();
    register_network_requests();
    configure_status_bar();
    return () => {};
  }, []);

  const is_auth = useSelector((state) => !!state.auth.is_auth);

  return is_auth ? <AppNavigator /> : <AuthNavigator />;
};

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer theme={DarkTheme}>
        <AppContainer />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
