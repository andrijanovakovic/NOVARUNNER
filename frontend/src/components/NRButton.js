import React from "react";
import { ActivityIndicator, Button, StyleSheet, View } from "react-native";

const NRButton = ({ onPress, title, color = "#2EC4B6", loading, buttonStyle, containerStyle }) => {
  return (
    <View style={[containerStyle]}>
      {loading ? (
        <ActivityIndicator color={"#FDFFFC"} size={"small"} />
      ) : (
        <Button onPress={onPress} title={title} color={color} style={[buttonStyle]} />
      )}
    </View>
  );
};

export default NRButton;

const styles = StyleSheet.create({});
