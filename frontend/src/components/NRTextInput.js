import React from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";

const NRTextInput = ({
  label = null,
  onChangeText,
  value = "",
  placeholder = "",
  textInputProps = {},
  mainContainerProps = {},
  labelProps = {},
  containerStyle = {},
  inputStyle = {},
}) => {
  return (
    <View style={[styles.main_container, containerStyle]} {...mainContainerProps}>
      {label ? (
        <Text style={styles.label_text} {...labelProps}>
          {label}
        </Text>
      ) : null}
      <TextInput style={[styles.input, inputStyle]} onChangeText={onChangeText} value={value} placeholder={placeholder} {...textInputProps} />
    </View>
  );
};

export default NRTextInput;

const styles = StyleSheet.create({
  main_container: { marginVertical: 5 },
  label_text: {
    color: "#FDFFFC",
  },
  input: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#ddd",
    color: "#fff",
  },
});
