import React from "react";
import { StyleSheet, Text, View, Switch } from "react-native";

const NRSwitch = ({ label, selected, onSelect, containerStyle = {}, labelStyle = {} }) => {
  return (
    <View style={[styles.main_container, containerStyle]}>
      <Text style={[styles.label, labelStyle]}>{label}</Text>
      <Switch style={styles.switch_style} trackColor={{ false: "#767577", true: "#00C176" }} onValueChange={() => onSelect()} value={selected} />
    </View>
  );
};

export default NRSwitch;

const styles = StyleSheet.create({
  main_container: {
    paddingVertical: 5,
    borderColor: "#ddd",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  label: { color: "#FDFFFC" },
  switch_style: {
    transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }],
  },
});
