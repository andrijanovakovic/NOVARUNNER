import React from "react";
import { StyleSheet, Text, View } from "react-native";

const ListEmptyComponent = ({ containerStyle = {}, labelStyle = {} }) => {
  return (
    <View style={[{ padding: 20 }, containerStyle]}>
      <Text style={[{ color: "#FDFFFC", textAlign: "center", fontWeight: "bold" }, containerStyle]}>Ni podatkov</Text>
    </View>
  );
};

export default ListEmptyComponent;

const styles = StyleSheet.create({});
