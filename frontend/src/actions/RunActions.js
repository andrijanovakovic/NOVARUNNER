import axios from "axios";
import SimpleToast from "react-native-simple-toast";
import { nr_as_get_object } from "../misc/async_storage";
import { show_error_from_request_to_user } from "../misc/helpers";
import {
  CHECK_FOR_UNFINISHED_TRAINING_FAIL,
  CHECK_FOR_UNFINISHED_TRAINING_LOADING,
  CHECK_FOR_UNFINISHED_TRAINING_SUCCESS,
  GET_LAST_TRAINING_FAIL,
  GET_LAST_TRAINING_LOADING,
  GET_LAST_TRAINING_SUCCESS,
  GET_PREVIOUS_RUNS_FAIL,
  GET_PREVIOUS_RUNS_LOADING,
  GET_PREVIOUS_RUNS_SUCCESS,
  START_TRAINING_FAIL,
  START_TRAINING_LOADING,
  START_TRAINING_SUCCESS,
  STOP_TRAINING_FAIL,
  STOP_TRAINING_LOADING,
  STOP_TRAINING_SUCCESS,
} from "../reducers/RunReducer";

export const get_previous_runs = (data) => {
  return async (dispatch) => {
    dispatch({ type: GET_PREVIOUS_RUNS_LOADING });

    try {
      let r = await axios.get("/run/previous-runs", { ...data });
      if (r && r.data && r.data.success) {
        dispatch({ type: GET_PREVIOUS_RUNS_SUCCESS, payload: r.data.data, stats: r.data.stats });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: GET_PREVIOUS_RUNS_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: GET_PREVIOUS_RUNS_FAIL });
    }
  };
};

export const stop_training = (data) => {
  return async (dispatch) => {
    dispatch({ type: STOP_TRAINING_LOADING });

    try {
      let r = await axios.post("/run/stop", { ...data });
      if (r && r.data && r.data.success) {
        dispatch({ type: STOP_TRAINING_SUCCESS, payload: r.data.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: STOP_TRAINING_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: STOP_TRAINING_FAIL });
    }
  };
};

export const check_for_unfinished_training = () => {
  return async (dispatch) => {
    dispatch({ type: CHECK_FOR_UNFINISHED_TRAINING_LOADING });

    try {
      let r = await axios.get("/run/unfinished", {});
      if (r && r.data && r.data.success) {
        dispatch({ type: CHECK_FOR_UNFINISHED_TRAINING_SUCCESS, payload: r.data.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: CHECK_FOR_UNFINISHED_TRAINING_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: CHECK_FOR_UNFINISHED_TRAINING_FAIL });
    }
  };
};

export const get_last_training = () => {
  return async (dispatch) => {
    dispatch({ type: GET_LAST_TRAINING_LOADING });

    try {
      let r = await axios.get("/run/last", {});
      if (r && r.data && r.data.success) {
        dispatch({ type: GET_LAST_TRAINING_SUCCESS, payload: r.data.last_run });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: GET_LAST_TRAINING_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: GET_LAST_TRAINING_FAIL });
    }
  };
};

export const start_training = (time_goal = null, distance_goal = null) => {
  return async (dispatch) => {
    dispatch({ type: START_TRAINING_LOADING });
    try {
      let r = await axios.post("/run/start", {
        target_duration_in_seconds: time_goal ? time_goal * 60 : null,
        target_distance_in_meters: distance_goal ? distance_goal * 1000 : null,
        datetime_started: new Date(),
      });
      if (r && r.data && r.data.success) {
        dispatch({ type: START_TRAINING_SUCCESS, payload: r.data.run });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: START_TRAINING_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: START_TRAINING_FAIL });
    }
  };
};

export const get_user_runs = async () => {
  let { _id } = await nr_as_get_object("user");

  try {
    let r = await axios.get("/run/all", { user_id: _id });
    return r.data;
  } catch (error) {
    console.log(error);
    SimpleToast.show(`Prišlo je do napake. (${error.message})`);
    return;
  }
};

export const start_run = async (run) => {
  let { _id } = await nr_as_get_object("user");

  try {
    let r = await axios.post("/run/start", { user_id: _id, ...run });
    return r.data;
  } catch (error) {
    console.log(error);
    SimpleToast.show(`Prišlo je do napake. (${error.message})`);
    return;
  }
};
