import axios from "axios";
import SimpleToast from "react-native-simple-toast";
import { show_error_from_request_to_user } from "../misc/helpers";
import { GET_USER_INFO_FAIL, GET_USER_INFO_LOADING, GET_USER_INFO_SUCCESS, SAVE_USER_INFO_FAIL, SAVE_USER_INFO_LOADING, SAVE_USER_INFO_SUCCESS } from "../reducers/UserReducer";

export const get_user_info = () => {
  return async (dispatch) => {
    dispatch({ type: GET_USER_INFO_LOADING });

    try {
      let r = await axios.get("/user-info/get", {});
      if (r && r.data && r.data.success) {
        dispatch({ type: GET_USER_INFO_SUCCESS, payload: r.data.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: GET_USER_INFO_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: GET_USER_INFO_FAIL });
    }
  };
};

export const save_user_info = (id, data) => {
  return async (dispatch) => {
    dispatch({ type: SAVE_USER_INFO_LOADING });

    try {
      let r = await axios.post("/user-info/save", { ...data, user_info_id: id });
      if (r && r.data && r.data.success) {
        dispatch({ type: SAVE_USER_INFO_SUCCESS, payload: r.data.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: SAVE_USER_INFO_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: SAVE_USER_INFO_FAIL });
    }
  };
};
