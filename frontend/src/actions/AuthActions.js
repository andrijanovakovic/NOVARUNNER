import { API_URL } from "@env";
import axios from "axios";
import SimpleToast from "react-native-simple-toast";
import { nr_as_set_object, nr_as_set_string } from "../misc/async_storage";
import { show_error_from_request_to_user } from "../misc/helpers";
import { login_user_fail, login_user_loading, login_user_success, LOGOUT_USER, register_user_fail, register_user_loading, register_user_success } from "../reducers/AuthReducer";

export const logout_user = () => {
  return async (dispatch) => {
    await nr_as_set_object("auto_login", false);
    await nr_as_set_string("jwt_token", "");
    dispatch({ type: LOGOUT_USER });
    dispatch({ type: "RESET_REDUCERS" });
  };
};

export const login_user = (identifier, password) => {
  return async (dispatch) => {
    dispatch({ type: login_user_loading });

    if (!identifier || identifier.trim() == "") {
      SimpleToast.show("Uporabniško ime ali e-naslov je obvezen podatek. Prosimo popravite.");
      dispatch({ type: login_user_fail });
      return;
    }

    if (!password || password.trim() == "") {
      SimpleToast.show("Geslo je obvezen podatek. Prosimo popravite.");
      dispatch({ type: login_user_fail });
      return;
    }

    try {
      const r = await axios.post("/user/login", {
        identifier,
        password,
      });

      if (r && r.data && r.data.success) {
        await nr_as_set_string("jwt_token", r.data.token);
        await nr_as_set_object("user", r.data.user);
        dispatch({ type: login_user_success, payload: r.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: login_user_fail });
      }
    } catch (error) {
      console.log(error);
      console.log(error.message);
      SimpleToast.show(`Prišlo je do napake pri prijavi uporabnika. (${error.message}) ${API_URL}`);
      dispatch({ type: login_user_fail });
    }
  };
};

export const register_user = (email = "", username = "", password = "", password_repeat = "") => {
  return async (dispatch) => {
    dispatch({ type: register_user_loading });

    if (email.trim() == "") {
      SimpleToast.show("E-naslov je obvezen podatek. Prosimo popravite.");
      dispatch({ type: register_user_fail });
      return;
    }
    if (username.trim() == "") {
      SimpleToast.show("Uporabniško ime je obvezen podatek. Prosimo popravite.");
      dispatch({ type: register_user_fail });
      return;
    }
    if (password.trim() == "") {
      SimpleToast.show("Geslo je obvezen podatek. Prosimo popravite.");
      dispatch({ type: register_user_fail });
      return;
    }
    if (password_repeat.trim() == "") {
      SimpleToast.show("Ponovitev gesla je obvezen podatek. Prosimo popravite.");
      dispatch({ type: register_user_fail });
      return;
    }
    if (password_repeat != password) {
      SimpleToast.show("Gesli se ne ujemata. Prosimo popravite.");
      dispatch({ type: register_user_fail });
      return;
    }

    try {
      const r = await axios.post("/user/register", {
        email,
        username,
        password,
        password_repeat,
      });

      if (r && r.data && r.data.success) {
        dispatch({ type: register_user_success, payload: r.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: register_user_fail });
      }
    } catch (error) {
      console.log(error);
      console.log(error.message);
      SimpleToast.show(`Prišlo je do napake pri registraciji uporabnika. (${error.message})`);
      dispatch({ type: register_user_fail });
    }
  };
};
