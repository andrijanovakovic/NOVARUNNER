import axios from "axios";
import SimpleToast from "react-native-simple-toast";
import { show_error_from_request_to_user } from "../misc/helpers";
import {
  CREATE_ROOM_FAIL,
  CREATE_ROOM_LOADING,
  CREATE_ROOM_SUCCESS,
  GET_ACTIVE_RUN_ROOMS_FAIL,
  GET_ACTIVE_RUN_ROOMS_LOADING,
  GET_ACTIVE_RUN_ROOMS_SUCCESS,
  GET_ROOM_DETAILS_FAIL,
  GET_ROOM_DETAILS_LOADING,
  GET_ROOM_DETAILS_SUCCESS,
  STOP_RUN_IN_ROOM_FAIL,
  STOP_RUN_IN_ROOM_LOADING,
  STOP_RUN_IN_ROOM_SUCCESS,
} from "../reducers/RunRoomReducer";

export const stop_run_in_room = (room_id = "") => {
  return async (dispatch) => {
    dispatch({ type: STOP_RUN_IN_ROOM_LOADING });

    try {
      let r = await axios.post("/run-room/deactivate", { room_id });
      if (r && r.data && r.data.success) {
        dispatch({ type: STOP_RUN_IN_ROOM_SUCCESS, payload: r.data.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: STOP_RUN_IN_ROOM_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: STOP_RUN_IN_ROOM_FAIL });
    }
  };
};

export const create_room = (room_data) => {
  return async (dispatch) => {
    dispatch({ type: CREATE_ROOM_LOADING });

    try {
      let r = await axios.post("/run-room/create", { ...room_data });
      if (r && r.data && r.data.success) {
        dispatch({ type: CREATE_ROOM_SUCCESS, payload: r.data.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: CREATE_ROOM_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: CREATE_ROOM_FAIL });
    }
  };
};

export const get_active_run_rooms = () => {
  return async (dispatch) => {
    dispatch({ type: GET_ACTIVE_RUN_ROOMS_LOADING });

    try {
      let r = await axios.get("/run-room/active-rooms", {});
      if (r && r.data && r.data.success) {
        dispatch({ type: GET_ACTIVE_RUN_ROOMS_SUCCESS, payload: r.data.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: GET_ACTIVE_RUN_ROOMS_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: GET_ACTIVE_RUN_ROOMS_FAIL });
    }
  };
};

export const get_room_details = (room_id = "") => {
  return async (dispatch) => {
    dispatch({ type: GET_ROOM_DETAILS_LOADING });

    try {
      let r = await axios.get("/run-room/room-details?room_id=" + room_id, {});
      if (r && r.data && r.data.success) {
        dispatch({ type: GET_ROOM_DETAILS_SUCCESS, payload: r.data.data });
      } else {
        show_error_from_request_to_user(r);
        dispatch({ type: GET_ROOM_DETAILS_FAIL });
      }
    } catch (error) {
      console.log(error);
      SimpleToast.show(`Prišlo je do napake. (${error.message})`);
      dispatch({ type: GET_ROOM_DETAILS_FAIL });
    }
  };
};
