import React, { Component } from "react";
import { View, Text, TouchableOpacity, RefreshControl, FlatList, StyleSheet } from "react-native";
import { connect } from "react-redux";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { get_active_run_rooms } from "../actions/RunRoomActions";
import ListEmptyComponent from "../components/ListEmptyComponent";
import moment from "moment";
import { nr_message_box } from "../misc/helpers";

export class RunRooms extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.get_active_run_rooms();
  }

  format_duration = (duration = 0) => {
    return (duration / 60 / 60).toFixed(2);
  };

  format_distance = (distance = 0) => {
    return (distance / 1000).toFixed(2);
  };

  handle_room_press = async (room) => {
    const { datetime_run_start } = room;

    const run_has_already_started = moment(datetime_run_start).isBefore(moment());

    if (run_has_already_started) {
      let are_you_sure = await nr_message_box("Opozorilo", "Trening je v teku. Ali ste prepričani da se želite pridružiti tej sobi?", [
        { text: "Da", should_resolve: true },
        { text: "Ne", should_resolve: false },
      ]);
      if (!are_you_sure) return;
    }

    this.props.navigation.navigate("RunRoomDetails", {
      component_params: {
        action: "preview",
        room,
      },
    });
  };

  render_single_room = ({ item, index }) => {
    return (
      <TouchableOpacity style={styles.room_container} onPress={() => this.handle_room_press(item)}>
        <View style={styles.room_row}>
          <View style={styles.room_row_flex_container}>
            <Text style={styles.row_label}>Soba</Text>
            <Text style={[styles.row_value, { color: "#2a9d8f" }]}>{item.name}</Text>
          </View>
          <View style={styles.room_row_flex_container}>
            <Text style={styles.row_label}>Št. uporabnikov</Text>
            <Text style={[styles.row_value, { color: "#e9c46a" }]}>{item.guest_users_ids.length + 1}</Text>
          </View>
        </View>
        <View style={styles.room_row}>
          <View style={[styles.room_row_flex_container, { marginTop: 15 }]}>
            <Text style={styles.row_label}>Začetek</Text>
            <Text style={[styles.row_value, { color: "#2a9d8f" }]}>{moment(item.datetime_run_start).format("DD.MM.YYYY HH:mm")}</Text>
          </View>
          <View style={[styles.room_row_flex_container, { marginTop: 15 }]}>
            <Text style={styles.row_label}>Privatno</Text>
            <Text style={[styles.row_value, { color: "#e9c46a" }]}>{item.room_is_private ? "DA" : "NE"}</Text>
          </View>
        </View>
        <View style={styles.room_row}>
          <View style={[styles.room_row_flex_container, { marginTop: 15 }]}>
            <Text style={styles.row_label}>Ciljno trajanje</Text>
            <Text style={[styles.row_value, { color: "#2a9d8f" }]}> {item.target_duration_in_seconds ? this.format_duration(item.target_duration_in_seconds) : "/"}</Text>
          </View>
          <View style={[styles.room_row_flex_container, { marginTop: 15 }]}>
            <Text style={styles.row_label}>Ciljna razdalja</Text>
            <Text style={[styles.row_value, { color: "#e9c46a" }]}>{item.target_distance_in_meters ? this.format_distance(item.target_distance_in_meters) : "/"}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const { active_run_rooms, active_run_rooms_loading } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          ref={(ref) => (this.list_ref = ref)}
          data={active_run_rooms}
          renderItem={(item) => this.render_single_room(item)}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={() => <ListEmptyComponent />}
          refreshControl={<RefreshControl tintColor={"#fff"} refreshing={active_run_rooms_loading} onRefresh={() => this.props.get_active_run_rooms()} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  room_container: { marginTop: 20, marginHorizontal: 20, borderRadius: 10, padding: 20, backgroundColor: "#2e2e2e" },
  room_row: { flexDirection: "row", justifyContent: "space-around" },
  row_label: { textAlign: "center", color: "#fdfffc" },
  row_value: { textAlign: "center", fontWeight: "200", fontSize: 22, color: "#fdfffc" },
  room_row_flex_container: { flex: 1, justifyContent: "center" },
});

const mapStateToProps = (state) => ({
  active_run_rooms: state.run_room.active_run_rooms,
  active_run_rooms_loading: state.run_room.active_run_rooms_loading,
});

const mapDispatchToProps = { get_active_run_rooms };

export default connect(mapStateToProps, mapDispatchToProps)(RunRooms);
