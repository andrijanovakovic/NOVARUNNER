import React, { Component } from "react";
import { ScrollView } from "react-native";
import { connect } from "react-redux";
import { get_user_info, save_user_info } from "../actions/UserActions";
import NRButton from "../components/NRButton";
import NRTextInput from "../components/NRTextInput";
import { object_is_empty } from "../misc/helpers";

export class EditUserInfo extends Component {
  constructor(props) {
    super(props);

    const { component_params = {} } = props.route.params;
    const { gender = "/", height_cm = "/", weight_kg = "/", date_of_birth = "/", name = "/", surname = "/", nickname = "/" } = component_params.user_info;

    this.state = {
      component_params,
      gender,
      height_cm,
      weight_kg,
      date_of_birth,
      name,
      surname,
      nickname,
      expect_save_user_info_response: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.expect_save_user_info_response && prevProps.save_user_info_loading && !this.props.save_user_info_loading) {
      this.setState({ expect_save_user_info_response: false }, () => {
        const { save_user_info_response } = this.props;
        if (!object_is_empty(save_user_info_response)) {
          this.props.get_user_info();
          this.props.navigation.pop();
        }
      });
    }
  }

  save_user_info = () => {
    const { gender, height_cm, weight_kg, date_of_birth, name, surname, nickname, component_params } = this.state;
    const { _id } = component_params.user_info;
    this.setState({ expect_save_user_info_response: true }, () => {
      this.props.save_user_info(_id, { gender, height_cm, weight_kg, date_of_birth, name, surname, nickname });
    });
  };

  render() {
    const { gender, height_cm, weight_kg, date_of_birth, name, surname, nickname } = this.state;
    return (
      <ScrollView contentContainerStyle={{ padding: 20 }}>
        <NRTextInput label={"Ime"} onChangeText={(value) => this.setState({ name: value })} value={name} textInputProps={{}} />
        <NRTextInput label={"Priimek"} onChangeText={(value) => this.setState({ surname: value })} value={surname} textInputProps={{}} />
        <NRTextInput label={"Vzdevek"} onChangeText={(value) => this.setState({ nickname: value })} value={nickname} textInputProps={{}} />
        <NRTextInput label={"Spol (M|F)"} onChangeText={(value) => this.setState({ gender: value })} value={gender} textInputProps={{}} />
        <NRTextInput label={"Datum rojstva (dd/mm/yyyy)"} onChangeText={(value) => this.setState({ date_of_birth: value })} value={date_of_birth} textInputProps={{}} />
        <NRTextInput
          label={"Višina (CM)"}
          onChangeText={(value) => this.setState({ height_cm: value })}
          value={height_cm && height_cm != "/" ? height_cm.toString() : "/"}
          textInputProps={{}}
        />
        <NRTextInput
          label={"Teža (KG)"}
          onChangeText={(value) => this.setState({ weight_kg: value })}
          value={weight_kg && weight_kg != "/" ? weight_kg.toString() : "/"}
          textInputProps={{}}
        />
        <NRButton title="Shrani" onPress={() => this.save_user_info()} />
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  save_user_info_loading: state.user.save_user_info_loading,
  save_user_info_response: state.user.save_user_info_response,
});

const mapDispatchToProps = { save_user_info, get_user_info };

export default connect(mapStateToProps, mapDispatchToProps)(EditUserInfo);
