import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import SimpleToast from "react-native-simple-toast";
import { connect } from "react-redux";
import { register_user } from "../actions/AuthActions";
import NRButton from "../components/NRButton";
import NRSwitch from "../components/NRSwitch";
import NRTextInput from "../components/NRTextInput";

export class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      password: "",
      password_repeat: "",
      secure_password_entry: true,
      expect_register_user_response: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.expect_register_user_response && prevProps.register_user_loading && !this.props.register_user_loading) {
      // user has tried to register, check response here
      this.setState({ expect_register_user_response: false }, () => {
        const { register_user_response } = this.props;
        if (register_user_response && register_user_response.success == true) {
          SimpleToast.show(register_user_response.message);
        }
      });
    }
  }

  handle_register_user_button_press = () => {
    const { email, username, password, password_repeat } = this.state;
    this.setState({ expect_register_user_response: true }, () => {
      this.props.register_user(email, username, password, password_repeat);
    });
  };

  render() {
    const { username, email, password, password_repeat, secure_password_entry } = this.state;
    const { register_user_loading } = this.props;

    return (
      <View style={styles.main_container}>
        <Text style={styles.heading_text}>REGISTRACIJA</Text>

        <NRTextInput
          label={"E-naslov"}
          onChangeText={(value) => this.setState({ email: value })}
          value={email}
          textInputProps={{
            autoCompleteType: "off",
            autoCapitalize: "none",
            autoCorrect: false,
          }}
        />

        <NRTextInput
          label={"Uporabniško ime"}
          onChangeText={(value) => this.setState({ username: value })}
          value={username}
          textInputProps={{
            autoCompleteType: "off",
            autoCapitalize: "none",
            autoCorrect: false,
          }}
        />

        <NRTextInput
          label={"Geslo"}
          onChangeText={(value) => this.setState({ password: value })}
          value={password}
          textInputProps={{
            autoCompleteType: "off",
            autoCapitalize: "none",
            secureTextEntry: secure_password_entry,
            autoCorrect: false,
          }}
        />

        <NRTextInput
          label={"Ponovitev gesla"}
          onChangeText={(value) => this.setState({ password_repeat: value })}
          value={password_repeat}
          textInputProps={{
            autoCompleteType: "off",
            autoCapitalize: "none",
            secureTextEntry: secure_password_entry,
            autoCorrect: false,
          }}
        />

        <NRSwitch
          label={"Prikaži geslo?"}
          labelStyle={{ color: "#FDFFFC" }}
          selected={!secure_password_entry}
          onSelect={() => this.setState({ secure_password_entry: !this.state.secure_password_entry })}
        />

        <NRButton
          onPress={() => this.handle_register_user_button_press()}
          title="Registracija"
          color="#FF9F1C"
          loading={register_user_loading}
          containerStyle={{ marginTop: 20 }}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  register_user_loading: state.auth.register_user_loading,
  register_user_response: state.auth.register_user_response,
});

const mapDispatchToProps = {
  register_user,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);

const styles = StyleSheet.create({
  input: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#ddd",
  },
  main_container: {
    flex: 1,
    padding: 20,
    justifyContent: "center",
  },
  description_text: {
    marginVertical: 20,
    color: "#FDFFFC",
    textAlign: "center",
  },
  heading_text: {
    textAlign: "center",
    color: "#FF9F1C",
    fontWeight: "bold",
    fontSize: 42,
    fontStyle: "italic",
    marginBottom: 20,
  },
  buttons_container: {
    flexDirection: "row",
    justifyContent: "center",
  },
});
