import { getPreciseDistance } from "geolib";
import moment from "moment";
import React, { Component } from "react";
import { ActivityIndicator, PermissionsAndroid, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Geolocation from "react-native-geolocation-service";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import SimpleToast from "react-native-simple-toast";
import { connect } from "react-redux";
import { check_for_unfinished_training, get_last_training, start_training, stop_training } from "../actions/RunActions";
import NRSwitch from "../components/NRSwitch";
import NRTextInput from "../components/NRTextInput";
import { nr_as_get_object } from "../misc/async_storage";
import { object_is_empty } from "../misc/helpers";

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // location permissions
      location_permission_loading: true,
      location_permission_granted: false,

      // current position and time
      current_position_loading: true,
      current_location: {},
      current_time: moment(),

      // goals
      save_selected_run_goals: false,
      time_goal_selected: false,
      distance_goal_selected: false,
      time_goal: null,
      distance_goal: null,

      // run settings
      run_started: false,
      show_run_statistics: false,

      // expect response in componentDidUpdate
      expect_start_training_response: false,
      expect_response_for_unfinished_training: false,
      expect_stop_training_response: false,

      // current run statistics
      current_run_time_elapsed_in_seconds: 0,
      current_run_time_elapsed_formatted: "",
      current_run_distance_in_meters: 0,
      current_run_speed_in_kmh: 0,
      current_run_max_speed_in_kmh: 0,
      current_run_pace_in_minutes_per_km: 0,
      current_run_average_speed_in_kmh: 0,
      current_run_polyline_coordinates: [],
      current_run_altitude_in_meters: 0,
      current_run_min_altitude_in_meters: 0,
      current_run_max_altitude_in_meters: 0,
    };
  }

  componentDidMount = () => {
    this.check_location_permission();
    this.set_time_interval();
    this.get_last_training();
    this.get_last_saved_goals();
    this.check_for_unfinished_training();
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.expect_start_training_response && prevProps.start_training_loading && !this.props.start_training_loading) {
      // start training response has loaded
      this.setState({ expect_start_training_response: false }, () => {
        const { start_training_response } = this.props;
        if (!object_is_empty(start_training_response)) {
          this.handle_run_started(start_training_response);
        }
      });
    }
    if (this.state.expect_response_for_unfinished_training && prevProps.check_for_unfinished_training_loading && !this.props.check_for_unfinished_training_loading) {
      // unfinished trainings response has loaded
      this.setState({ expect_response_for_unfinished_training: false }, () => {
        const { check_for_unfinished_training_response } = this.props;
        if (!object_is_empty(check_for_unfinished_training_response)) {
          this.handle_run_started(check_for_unfinished_training_response);
        }
      });
    }
    if (this.state.expect_stop_training_response && prevProps.stop_training_loading && !this.props.stop_training_loading) {
      // training has been stopped
      this.setState(
        {
          expect_stop_training_response: false,
          current_run_time_elapsed_in_seconds: 0,
          current_run_time_elapsed_formatted: "",
          current_run_distance_in_meters: 0,
          current_run_speed_in_kmh: 0,
          current_run_max_speed_in_kmh: 0,
          current_run_pace_in_minutes_per_km: 0,
          current_run_average_speed_in_kmh: 0,
          current_run_polyline_coordinates: [],
          current_run_altitude_in_meters: 0,
          current_run_min_altitude_in_meters: 0,
          current_run_max_altitude_in_meters: 0,
        },
        () => {
          const { stop_training_response } = this.props;
          if (!object_is_empty(stop_training_response)) {
            this.handle_run_been_has_stopped();
          }
        },
      );
    }
  }

  componentWillUnmount = () => {
    this.clear_time_interval();
    this.clear_run_time_interval();
  };

  handle_run_been_has_stopped = () => {
    this.setState({ show_run_statistics: false }, () => {
      SimpleToast.show("Trening končan!");
    });
  };

  check_for_unfinished_training = () => {
    this.setState({ expect_response_for_unfinished_training: true }, () => {
      this.props.check_for_unfinished_training();
    });
  };

  clear_run_time_interval = () => {
    if (this.current_run_time_elapsed_interval) {
      clearInterval(this.current_run_time_elapsed_interval);
    }
  };

  calculate_current_run_time_elapsed = () => {
    const { datetime_started } = this.state.run;
    let _datetime_started = moment(datetime_started);
    let _current_datetime = moment();
    let _diff = _current_datetime.diff(_datetime_started, "seconds");
    let _diff_formated = moment.duration(_diff, "seconds").format("HH:mm:ss", { trim: false });
    this.setState({ current_run_time_elapsed_in_seconds: _diff, current_run_time_elapsed_formatted: _diff_formated });
  };

  register_run_time_interval = () => {
    this.current_run_time_elapsed_interval = setInterval(this.calculate_current_run_time_elapsed, 1000);
  };

  handle_run_started = (run) => {
    this.setState({ run_started: true, show_run_statistics: true, run }, async () => {
      this.register_run_time_interval();
    });
  };

  get_last_saved_goals = async () => {
    let save_selected_run_goals = await nr_as_get_object("save_selected_run_goals");
    if (save_selected_run_goals) {
      let distance_goal_selected = await nr_as_get_object("distance_goal_selected");
      let distance_goal = await nr_as_get_object("distance_goal");
      let time_goal_selected = await nr_as_get_object("time_goal_selected");
      let time_goal = await nr_as_get_object("time_goal");
      this.setState({ distance_goal_selected, distance_goal, time_goal_selected, time_goal, save_selected_run_goals });
    }
  };

  get_last_training = () => {
    this.props.get_last_training();
  };

  clear_time_interval = () => {
    clearInterval(this.time_interval);
  };

  set_time_interval = () => {
    this.time_interval = setInterval(this.update_time, 1000);
  };

  update_time = () => {
    this.setState({ current_time: moment() });
  };

  check_location_permission = () => {
    this.setState({ location_permission_loading: true }, async () => {
      if (Platform.OS == "android") {
        const granted = (await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)) == PermissionsAndroid.RESULTS.GRANTED;
        this.setState({ location_permission_loading: false, location_permission_granted: granted }, () => {
          if (granted) {
            this.get_current_position();
          }
        });
      } else {
        this.setState({ location_permission_loading: false, location_permission_granted: true }, () => this.get_current_position());
      }
    });
  };

  get_current_position = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        this.setState({ current_location: { latitude, longitude }, current_position_loading: false });
      },
      (error) => {
        // See error code charts below.
        console.log(error.code, error.message);
        SimpleToast.show(error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
    );
  };

  handle_location_change = (c) => {
    if (this.state.run_started) {
      const { longitude: old_longitude, latitude: old_latitude } = this.state.current_location;
      const { longitude: new_longitude, latitude: new_latitude, speed } = c;

      // calculate distance
      let distance_between_old_loc_and_new_loc = getPreciseDistance(
        { latitude: old_latitude, longitude: old_longitude }, // old location
        { latitude: new_latitude, longitude: new_longitude }, /// new location
      );

      let { current_run_distance_in_meters, current_run_max_speed_in_kmh, current_run_time_elapsed_in_seconds } = this.state;

      // calculate new total distance
      let _current_run_distance_in_meters = Number((current_run_distance_in_meters + distance_between_old_loc_and_new_loc).toFixed(2));
      let _current_run_speed_in_kmh = Number((speed * 3.6).toFixed(2));

      // check if current speed is greater than max speed
      if (_current_run_speed_in_kmh > current_run_max_speed_in_kmh) {
        current_run_max_speed_in_kmh = _current_run_speed_in_kmh;
      }

      let current_run_time_elapsed_in_minutes = current_run_time_elapsed_in_seconds / 60;
      let current_run_distance_in_kilometers = _current_run_distance_in_meters / 1000;
      let _current_run_pace_in_minutes_for_km = Number((current_run_time_elapsed_in_minutes / current_run_distance_in_kilometers).toFixed(2));
      let _current_run_average_speed_in_kmh = Number(((_current_run_distance_in_meters / current_run_time_elapsed_in_seconds) * 3.6).toFixed(2));

      let { current_run_min_altitude_in_meters, current_run_max_altitude_in_meters } = this.state;
      if (c.altitude < current_run_min_altitude_in_meters) current_run_min_altitude_in_meters = c.altitude;
      else if (c.altitde > current_run_max_altitude_in_meters) current_run_max_altitude_in_meters = c.altitude;

      this.setState({
        current_run_distance_in_meters: _current_run_distance_in_meters,
        current_run_speed_in_kmh: _current_run_speed_in_kmh,
        current_run_max_speed_in_kmh,
        current_run_pace_in_minutes_per_km: _current_run_pace_in_minutes_for_km,
        current_run_average_speed_in_kmh: _current_run_average_speed_in_kmh,
        current_run_polyline_coordinates: [...this.state.current_run_polyline_coordinates, { longitude: new_longitude, latitude: new_latitude }],
        current_run_min_altitude_in_meters,
        current_run_max_altitude_in_meters,
        current_run_altitude_in_meters: c.altitude,
        current_location: c,
      });
    } else {
      this.setState({ current_location: c });
    }
  };

  render_current_time = () => {
    const { current_time } = this.state;
    return <Text style={styles.time_style}>{current_time.format("HH:mm:ss")}</Text>;
  };

  render_last_training_time = () => {
    const { last_training_data, get_last_training_loading } = this.props;
    const { run_started, run } = this.state;

    if (get_last_training_loading) {
      return null;
    }

    if (run_started) {
      return <Text style={[styles.time_style, { marginBottom: 0 }]}>Trening štartan {moment(run.datetime_started).format("DD.MM HH:mm:ss")}!</Text>;
    }

    if (!last_training_data) {
      return <Text style={[styles.time_style, { marginBottom: 0 }]}>Čas je za prvi trening!</Text>;
    }

    const { datetime_started } = last_training_data;

    return <Text style={[styles.time_style, { marginBottom: 0 }]}>Zadnji trening: {moment(datetime_started).format("DD.MM.YYYY HH:mm:ss")}</Text>;
  };

  render_run_statistics = () => {
    const { show_run_statistics } = this.state;

    if (!show_run_statistics) return null;

    const {
      current_run_distance_in_meters,
      current_run_time_elapsed_formatted,
      current_run_speed_in_kmh,
      current_run_pace_in_minutes_per_km,
      current_run_average_speed_in_kmh,
      current_run_max_speed_in_kmh,
    } = this.state;
    const { stop_training_loading } = this.props;

    return (
      <View style={[styles.bottom_half_container, { backgroundColor: "#ffba08" }]}>
        {stop_training_loading ? (
          <ActivityIndicator color={"#000"} />
        ) : (
          <>
            <Text style={styles.run_stats_label}>TRENING INFO</Text>

            <View style={styles.separator} />

            <View style={styles.run_stats_data_container}>
              <View style={styles.run_stats_row_container}>
                <Text style={styles.run_stats_row_label}>Čas</Text>
                <Text style={styles.run_stats_row_value}>{current_run_time_elapsed_formatted}</Text>
              </View>
              <View style={styles.run_stats_row_container}>
                <Text style={styles.run_stats_row_label}>Hitrost</Text>
                <Text style={styles.run_stats_row_value}>{current_run_speed_in_kmh} KM/H</Text>
              </View>
              <View style={styles.run_stats_row_container}>
                <Text style={styles.run_stats_row_label}>Max hitrost</Text>
                <Text style={styles.run_stats_row_value}>{current_run_max_speed_in_kmh} KM/H</Text>
              </View>
              <View style={styles.run_stats_row_container}>
                <Text style={styles.run_stats_row_label}>Povprečna hitrost</Text>
                <Text style={styles.run_stats_row_value}>{current_run_average_speed_in_kmh} KM/H</Text>
              </View>
              <View style={styles.run_stats_row_container}>
                <Text style={styles.run_stats_row_label}>Tempo</Text>
                <Text style={styles.run_stats_row_value}>{current_run_pace_in_minutes_per_km} Min/KM</Text>
              </View>
              <View style={styles.run_stats_row_container}>
                <Text style={styles.run_stats_row_label}>Razdalja</Text>
                <Text style={styles.run_stats_row_value}>{current_run_distance_in_meters} m</Text>
              </View>
            </View>
          </>
        )}
      </View>
    );
  };

  render_goals = () => {
    const { time_goal_selected, time_goal, distance_goal_selected, distance_goal, save_selected_run_goals, show_run_statistics } = this.state;
    const { start_training_loading } = this.props;

    if (show_run_statistics) return null;

    return (
      <View style={[styles.bottom_half_container, { backgroundColor: "#1a1a1a" }]}>
        {start_training_loading ? (
          <ActivityIndicator color={"#fff"} />
        ) : (
          <>
            <Text style={[styles.run_stats_label, { color: "#fdfffc" }]}>IZBERITE CILJE</Text>

            <View style={[styles.separator, { borderBottomColor: "#fdfffc" }]} />

            <NRSwitch
              containerStyle={{ padding: 0, margin: 0, paddingVertical: 0 }}
              label={time_goal_selected ? "Čas (v minutah)" : "Čas"}
              labelStyle={{ color: "#FDFFFC" }}
              selected={time_goal_selected}
              onSelect={() => this.setState({ time_goal_selected: !time_goal_selected })}
            />
            {time_goal_selected ? (
              <NRTextInput
                containerStyle={{ margin: 0 }}
                inputStyle={{ margin: 0, padding: 0 }}
                label={""}
                onChangeText={(value) => this.setState({ time_goal: value })}
                value={time_goal}
                textInputProps={{}}
              />
            ) : null}
            <NRSwitch
              containerStyle={{ padding: 0, margin: 0, paddingVertical: 0 }}
              label={distance_goal_selected ? "Razdalja (v kilometrih)" : "Razdalja"}
              labelStyle={{ color: "#FDFFFC" }}
              selected={distance_goal_selected}
              onSelect={() => this.setState({ distance_goal_selected: !distance_goal_selected })}
            />
            {distance_goal_selected ? (
              <NRTextInput
                containerStyle={{ margin: 0 }}
                inputStyle={{ margin: 0, padding: 0 }}
                label={""}
                onChangeText={(value) => this.setState({ distance_goal: value })}
                value={distance_goal}
                textInputProps={{}}
              />
            ) : null}
            <NRSwitch
              containerStyle={{ padding: 0, margin: 0, paddingVertical: 0 }}
              label={"Zapomni nastavljene cilje?"}
              labelStyle={{ color: "#FDFFFC" }}
              selected={save_selected_run_goals}
              onSelect={() => this.setState({ save_selected_run_goals: !save_selected_run_goals })}
            />
          </>
        )}
      </View>
    );
  };

  start_training = () => {
    const { time_goal, distance_goal } = this.state;
    this.setState({ expect_start_training_response: true }, () => {
      this.props.start_training(time_goal, distance_goal);
    });
  };

  stop_training = () => {
    this.setState({ run_started: false, expect_stop_training_response: true }, () => {
      this.clear_run_time_interval();

      const {
        current_run_time_elapsed_in_seconds,
        current_run_time_elapsed_formatted,
        current_run_distance_in_meters,
        current_run_speed_in_kmh,
        current_run_max_speed_in_kmh,
        current_run_pace_in_minutes_per_km,
        current_run_average_speed_in_kmh,
        current_run_polyline_coordinates,
        current_run_altitude_in_meters,
        current_run_min_altitude_in_meters,
        current_run_max_altitude_in_meters,
        run,
      } = this.state;

      let current_run_overall_altitude_change_in_meters = Math.abs(current_run_min_altitude_in_meters - current_run_max_altitude_in_meters);

      this.props.stop_training({
        current_run_time_elapsed_in_seconds,
        current_run_time_elapsed_formatted,
        current_run_distance_in_meters,
        current_run_speed_in_kmh,
        current_run_max_speed_in_kmh,
        current_run_pace_in_minutes_per_km,
        current_run_average_speed_in_kmh,
        current_run_polyline_coordinates,
        current_run_altitude_in_meters,
        current_run_min_altitude_in_meters,
        current_run_max_altitude_in_meters,
        current_run_overall_altitude_change_in_meters,
        run_id: run._id,
      });
    });
  };

  render_start_stop_button = () => {
    const { run_started, expect_response_for_unfinished_training } = this.state;
    const { start_training_loading, stop_training_loading } = this.props;

    if (expect_response_for_unfinished_training) {
      return (
        <TouchableOpacity style={[styles.start_stop_button, { borderColor: "#ddd" }]}>
          <Text style={[styles.heading_text, { color: "#ddd" }]}>NALAGAM...</Text>
        </TouchableOpacity>
      );
    }

    if (run_started) {
      return (
        <TouchableOpacity style={[styles.start_stop_button, { borderColor: "#b72731" }]} onPress={() => this.stop_training()}>
          <Text style={[styles.heading_text, { color: "#b72731" }]}>{stop_training_loading ? "NALAGAM..." : "STOP"}</Text>
        </TouchableOpacity>
      );
    }

    return (
      <TouchableOpacity style={[styles.start_stop_button, { borderColor: "#00ae5b" }]} onPress={() => this.start_training()}>
        <Text style={[styles.heading_text, { color: "#00ae5b" }]}>{start_training_loading ? "NALAGAM..." : "START"}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { current_location, location_permission_granted, location_permission_loading, current_position_loading } = this.state;

    if (location_permission_loading || current_position_loading) {
      return (
        <View style={styles.spinner_container}>
          <ActivityIndicator />
        </View>
      );
    }

    if (!location_permission_granted) {
      return (
        <View style={styles.spinner_container}>
          <Text>Prosimo, dovolite aplikaciji uporabo lokacijskih storitev.</Text>
        </View>
      );
    }

    return (
      <ScrollView contentContainerStyle={styles.other_content_container}>
        <MapView
          provider={PROVIDER_GOOGLE}
          showsUserLocation={true}
          userLocationPriority={"high"}
          followsUserLocation={true}
          userInterfaceStyle={"dark"}
          region={{
            latitude: current_location.latitude - 0.001 || 37.78825,
            longitude: current_location.longitude || -122.4324,
            latitudeDelta: 0.004,
            longitudeDelta: 0.005,
          }}
          style={styles.map}
          onUserLocationChange={(e) => this.handle_location_change(e.nativeEvent.coordinate)}
        ></MapView>

        <View style={styles.bottom_container}>
          {this.render_start_stop_button()}
          {this.render_current_time()}
          {this.render_goals()}
          {this.render_run_statistics()}
          {this.render_last_training_time()}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  get_last_training_loading: state.run.get_last_training_loading,
  last_training_data: state.run.last_training_data,
  start_training_loading: state.run.start_training_loading,
  start_training_response: state.run.start_training_response,
  check_for_unfinished_training_loading: state.run.check_for_unfinished_training_loading,
  check_for_unfinished_training_response: state.run.check_for_unfinished_training_response,
  stop_training_loading: state.run.stop_training_loading,
  stop_training_response: state.run.stop_training_response,
});

const mapDispatchToProps = { get_last_training, start_training, check_for_unfinished_training, stop_training };

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
  bottom_container: { position: "absolute", bottom: 0, left: 0, right: 0, backgroundColor: "rgba(26,26,26,0.5)", padding: 10, margin: 5, borderRadius: 10 },
  time_style: { marginVertical: 5, textAlign: "center", color: "#000" },
  bottom_half_container: { padding: 10, borderRadius: 10 },
  map_container: { margin: 10, borderRadius: 10, overflow: "hidden" },
  other_content_container: { flexGrow: 1 },
  start_stop_button: { borderRadius: 10, borderWidth: 1, backgroundColor: "#1a1a1a" },
  spinner_container: { flex: 1, padding: 20, justifyContent: "center" },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  heading_text: {
    textAlign: "center",
    color: "#000",
    fontWeight: Platform.OS == "android" ? "bold" : "100",
    fontSize: 42,
  },
  separator: {
    borderBottomWidth: 0.5,
    borderBottomColor: "#000",
  },
  run_stats_label: { textAlign: "center", fontSize: 16, fontWeight: "bold", marginBottom: 5 },
  run_stats_data_container: { padding: 10 },
  run_stats_row_container: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginBottom: 3 },
  run_stats_row_label: { textAlign: "center", fontSize: 16, fontWeight: "bold" },
  run_stats_row_value: { textAlign: "center", fontSize: 12 },
});
