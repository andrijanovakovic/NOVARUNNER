import React, { Component } from "react";
import { ActivityIndicator, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { logout_user } from "../actions/AuthActions";
import { get_user_info } from "../actions/UserActions";
import NRButton from "../components/NRButton";

export class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.get_user_info();
  }

  handle_edit_user_info_pop = (should_refresh = false) => {
    if (should_refresh) {
      this.props.get_user_info();
    }
  };

  edit_user_info = () => {
    const { user_info } = this.props;
    this.props.navigation.navigate("EditUserInfo", {
      component_params: {
        user_info,
      },
    });
  };

  render() {
    const { user_info_loading, user_info } = this.props;

    if (user_info_loading) {
      <View style={styles.spinner_container}>
        <ActivityIndicator />
      </View>;
    }

    const { gender = "/", height_cm = "/", weight_kg = "/", date_of_birth = "/", name = "/", surname = "/", nickname = "/" } = user_info;

    return (
      <ScrollView>
        <TouchableOpacity style={styles.data_container} onPress={() => this.edit_user_info()}>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Ime</Text>
            <Text style={[styles.row_value_text, { color: "#f77f00" }]}>{name}</Text>
          </View>

          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Priimek</Text>
            <Text style={[styles.row_value_text, { color: "#d62828" }]}>{surname}</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Vzdevek</Text>
            <Text style={[styles.row_value_text, { color: "#fcbf49" }]}>{nickname}</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={styles.data_container} onPress={() => this.edit_user_info()}>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Spol</Text>
            <Text style={[styles.row_value_text, { color: "#f77f00" }]}>{gender}</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Datum rojstva (dd/mm/yyyy)</Text>
            <Text style={[styles.row_value_text, { color: "#d62828" }]}>{date_of_birth}</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Višina (CM)</Text>
            <Text style={[styles.row_value_text, { color: "#fcbf49" }]}>{height_cm}</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Teža (KG)</Text>
            <Text style={[styles.row_value_text, { color: "#90e0ef" }]}>{weight_kg}</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.data_container}>
          <NRButton title="Odjava" onPress={() => this.props.logout_user()} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  data_container: {
    marginTop: 20,
    marginHorizontal: 20,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "#1a1a1a",
  },
  row_container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  row_label_text: {
    flex: 1.5,
    color: "#fdfffc",
  },
  row_value_text: {
    textAlign: "right",
    flex: 2,
    fontSize: 32,
    fontWeight: "100",
    color: "#fdfffc",
  },
  spinner_container: { flex: 1, padding: 20, justifyContent: "center" },
});

const mapStateToProps = (state) => ({
  user_info_loading: state.user.user_info_loading,
  user_info: state.user.user_info,
});

const mapDispatchToProps = { get_user_info, logout_user };

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
