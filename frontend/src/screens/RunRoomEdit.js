import moment from "moment";
import React, { Component } from "react";
import { ScrollView } from "react-native";
import SimpleToast from "react-native-simple-toast";
import { connect } from "react-redux";
import { create_room, get_active_run_rooms } from "../actions/RunRoomActions";
import NRButton from "../components/NRButton";
import NRSwitch from "../components/NRSwitch";
import NRTextInput from "../components/NRTextInput";
import { object_is_empty } from "../misc/helpers";

/**
 * string - ime sobe
 * bool - room_is_private
 */

export class RunRoomEdit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      room_name: "",
      room_is_private: true,
      datetime_training_start: moment().format("DD.MM.YYYY HH:mm"),
      guests: "",
      distance_goal: "",
      duration_goal: "",
      expect_create_room_response: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.expect_create_room_response && prevProps.create_room_loading && !this.props.create_room_loading) {
      this.setState({ expect_create_room_response: false }, () => {
        const { create_room_response } = this.props;
        if (!object_is_empty(create_room_response)) {
          this.props.navigation.pop();
          this.props.get_active_run_rooms();
        }
      });
    }
  }

  create_room = () => {
    const { room_name, room_is_private, datetime_training_start, guests, distance_goal, duration_goal } = this.state;

    if (!room_name || room_name == "") {
      SimpleToast.show("Ime sobe je obvezen podatek, prosimo popravite.");
      return;
    }

    if (!datetime_training_start) {
      SimpleToast.show("Datum in čas začetka je pobvezen podatek, prosimo popravite.");
      return;
    }

    let _guests = guests.split(",");

    this.setState({ expect_create_room_response: true }, () => {
      this.props.create_room({
        room_name,
        room_is_private,
        datetime_run_start: moment(datetime_training_start, "DD.MM.YYYY HH:mm"),
        guest_users_identifiers: _guests,
        target_distance_in_meters: ((distance_goal || 0) / 1000).toFixed(2),
        target_duration_in_seconds: ((duration_goal || 0) * 60).toFixed(2),
      });
    });
  };

  render() {
    return (
      <ScrollView contentContainerStyle={{ padding: 20 }}>
        <NRTextInput label={"Ime sobe"} onChangeText={(value) => this.setState({ room_name: value })} value={this.state.room_name} />
        <NRSwitch label={"Privatna soba?"} selected={this.state.room_is_private} onSelect={() => this.setState({ room_is_private: !this.state.room_is_private })} />
        <NRTextInput
          label={"Datum in čas začetka (DD.MM.YYYY HH:mm)"}
          onChangeText={(value) => this.setState({ datetime_training_start: value })}
          value={this.state.datetime_training_start}
        />
        <NRTextInput label={"Cilj 1 - čas (v minutah)"} onChangeText={(value) => this.setState({ duration_goal: value })} value={this.state.duration_goal} />
        <NRTextInput label={"Cilj 2 - razdalja (v kilometrih)"} onChangeText={(value) => this.setState({ distance_goal: value })} value={this.state.distance_goal} />
        {this.state.room_is_private ? (
          <NRTextInput
            label={"Gosti"}
            onChangeText={(value) => this.setState({ guests: value })}
            value={this.state.guests}
            textInputProps={{
              autoCompleteType: "off",
              autoCorrect: false,
              autoCapitalize: "none",
            }}
          />
        ) : null}
        <NRButton title="Shrani" onPress={() => this.create_room()} />
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  create_room_loading: state.run_room.create_room_loading,
  create_room_response: state.run_room.create_room_response,
});

const mapDispatchToProps = { create_room, get_active_run_rooms };

export default connect(mapStateToProps, mapDispatchToProps)(RunRoomEdit);
