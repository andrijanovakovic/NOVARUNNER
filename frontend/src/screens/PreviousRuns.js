import moment from "moment";
import React, { Component } from "react";
import { FlatList, RefreshControl, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { get_previous_runs } from "../actions/RunActions";
import ListEmptyComponent from "../components/ListEmptyComponent";

export class PreviousRuns extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.get_previous_runs();
  }

  render_separator() {
    return <View style={{ height: 10, backgroundColor: "#f7f7f7" }} />;
  }

  view_run_details = (run) => {
    this.props.navigation.navigate("RunDetails", {
      component_params: {
        run,
      },
    });
  };

  render_single_run = ({ item, index }) => {
    return (
      <TouchableOpacity key={index} style={styles.single_run_container} onPress={() => this.view_run_details(item)}>
        <View style={{ flexDirection: "row" }}>
          <View style={styles.single_run_left_half_container}>
            <Text style={{ color: "#2EC4B6", textAlign: "center", fontWeight: "bold", fontSize: 15 }}>{item.distance_in_meters} m</Text>
            <Text style={{ color: "#FF9F1C", textAlign: "center", fontWeight: "bold", fontSize: 15 }}>{item.average_speed_in_kmh} KM/H</Text>
          </View>
          <View style={styles.single_run_right_half_container}>
            <View style={styles.data_row_container}>
              <Text style={styles.data_row_label}>Začetek</Text>
              <Text style={styles.data_row_value}>{moment(item.datetime_started).format("DD.MM.YYYY HH:mm:ss")}</Text>
            </View>
            <View style={styles.data_row_container}>
              <Text style={styles.data_row_label}>Konec</Text>
              <Text style={styles.data_row_value}>{moment(item.datetime_ended).format("DD.MM.YYYY HH:mm:ss")}</Text>
            </View>
            <View style={styles.data_row_container}>
              <Text style={styles.data_row_label}>Trajanje</Text>
              <Text style={styles.data_row_value}>{item.duration_formatted_as_hh_mm_ss}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  get_hours_from_seconds = (sec = 0) => {
    return Number(sec / 3600).toFixed(2);
  };

  get_km_from_m = (m = 0) => {
    return Number(m / 1000).toFixed(2);
  };

  render_stats = () => {
    const { previous_runs_stats } = this.props;
    const {
      total_run_meters,
      total_run_time_in_seconds,
      average_speed_in_kmh,
      average_pace_in_minutes_per_km,
      max_speed_in_kmh,
      fastest_pace_in_minutes_per_km,
    } = previous_runs_stats;

    return (
      <View style={styles.stats_container}>
        <View style={styles.stats_row}>
          <View style={styles.stats_row_flex_container}>
            <Text style={styles.stats_row_label}>Aktivnih KM</Text>
            <Text style={[styles.stats_row_value, { color: "#2a9d8f" }]}>{this.get_km_from_m(total_run_meters)} KM</Text>
          </View>
          <View style={styles.stats_row_flex_container}>
            <Text style={styles.stats_row_label}>Aktivnih ur</Text>
            <Text style={[styles.stats_row_value, { color: "#e9c46a" }]}>{this.get_hours_from_seconds(total_run_time_in_seconds)} H</Text>
          </View>
        </View>
        <View style={styles.stats_row}>
          <View style={[styles.stats_row_flex_container, { marginTop: 15 }]}>
            <Text style={styles.stats_row_label}>Povprečna hitrost</Text>
            <Text style={[styles.stats_row_value, { color: "#2a9d8f" }]}>{average_speed_in_kmh} KM/H</Text>
          </View>
          <View style={[styles.stats_row_flex_container, { marginTop: 15 }]}>
            <Text style={styles.stats_row_label}>Tempo</Text>
            <Text style={[styles.stats_row_value, { color: "#e9c46a" }]}>{average_pace_in_minutes_per_km} Min/KM</Text>
          </View>
        </View>
        <View style={styles.stats_row}>
          <View style={[styles.stats_row_flex_container, { marginTop: 15 }]}>
            <Text style={styles.stats_row_label}>Max hitrost</Text>
            <Text style={[styles.stats_row_value, { color: "#2a9d8f" }]}>{max_speed_in_kmh} KM/H</Text>
          </View>
          <View style={[styles.stats_row_flex_container, { marginTop: 15 }]}>
            <Text style={styles.stats_row_label}>Najboljši tempo</Text>
            <Text style={[styles.stats_row_value, { color: "#e9c46a" }]}>{fastest_pace_in_minutes_per_km} Min/KM</Text>
          </View>
        </View>
      </View>
    );
  };

  render() {
    const { previous_runs, previous_runs_loading } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          contentContainerStyle={{ paddingBottom: 20 }}
          ref={(ref) => (this.list_ref = ref)}
          ListHeaderComponent={() => this.render_stats()}
          data={previous_runs}
          renderItem={(item) => this.render_single_run(item)}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={() => <ListEmptyComponent />}
          refreshControl={<RefreshControl tintColor={"#fff"} refreshing={previous_runs_loading} onRefresh={() => this.props.get_previous_runs()} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  data_row_container: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginBottom: 2 },
  data_row_label: { textAlign: "center", fontSize: 13, fontWeight: "bold", color: "#FDFFFC" },
  data_row_value: { textAlign: "center", fontSize: 12, color: "#FDFFFC" },
  single_run_container: {
    padding: 10,
    marginTop: 20,
    marginHorizontal: 20,
    borderRadius: 10,
    backgroundColor: "#1a1a1a",
  },
  single_run_left_half_container: { flex: 1, padding: 5, justifyContent: "center", alignItems: "center" },
  single_run_right_half_container: { flex: 3, padding: 5, justifyContent: "center" },
  stats_container: { marginTop: 20, marginHorizontal: 20, borderRadius: 10, padding: 20, backgroundColor: "#2e2e2e" },
  stats_row: { flexDirection: "row", justifyContent: "space-around" },
  stats_row_label: { textAlign: "center", color: "#fdfffc" },
  stats_row_value: { textAlign: "center", fontWeight: "200", fontSize: 22, color: "#fdfffc" },
  stats_row_flex_container: { flex: 1, justifyContent: "center" },
});

const mapStateToProps = (state) => ({
  previous_runs_loading: state.run.previous_runs_loading,
  previous_runs: state.run.previous_runs,
  previous_runs_stats: state.run.previous_runs_stats,
});

const mapDispatchToProps = { get_previous_runs };

export default connect(mapStateToProps, mapDispatchToProps)(PreviousRuns);
