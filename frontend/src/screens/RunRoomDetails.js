import { WS_URL } from "@env";
import { getPreciseDistance } from "geolib";
import moment from "moment";
import React, { Component } from "react";
import { ActivityIndicator, PermissionsAndroid, Platform, ScrollView, StyleSheet, Text, View } from "react-native";
import Geolocation from "react-native-geolocation-service";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import SimpleToast from "react-native-simple-toast";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";
import io from "socket.io-client";
import { get_room_details, stop_run_in_room, get_active_run_rooms } from "../actions/RunRoomActions";
import NRButton from "../components/NRButton";
import { nr_as_get_object } from "../misc/async_storage";
import { object_is_empty } from "../misc/helpers";

export class RunRoomDetails extends Component {
  constructor(props) {
    super(props);

    const { component_params = {} } = props.route.params;

    this.state = {
      component_params,
      expect_room_details_response: true,
      run_has_already_started: false,
      time_display: "",
      check_if_run_has_already_started_loading: true,

      time_entered_room: moment(),
      run_has_been_stopped: false,
      datetime_run_stop: null,

      // location permissions
      location_permission_loading: true,
      location_permission_granted: false,

      // current position and time
      current_position_loading: true,
      current_location: {},

      // current run statistics
      current_run_time_elapsed_in_seconds: 0,
      current_run_distance_in_meters: 0,
      current_run_speed_in_kmh: 0,
      current_run_max_speed_in_kmh: 0,
      current_run_pace_in_minutes_per_km: 0,
      current_run_average_speed_in_kmh: 0,
      current_run_polyline_coordinates: [],
      current_run_altitude_in_meters: 0,
      current_run_min_altitude_in_meters: 0,
      current_run_max_altitude_in_meters: 0,
      stats_per_user: {},
    };
  }

  componentDidMount = () => {
    this.check_location_permission();
    this.get_data();
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.expect_room_details_response && prevProps.room_details_loading && !this.props.room_details_loading) {
      this.setState({ expect_room_details_response: false }, () => {
        const { room_details } = this.props;
        if (!object_is_empty(room_details)) {
          this.prepare_room_details();
        }
      });
    }
  }

  componentWillUnmount = () => {
    this.props.get_active_run_rooms();
    if (this.ws) {
      this.ws.emit("leave_room", {
        user_id: this.state.user._id,
        room_id: this.state.component_params.room._id,
      });
    }
    this.clear_time_interval();
    this.ws = null;
  };

  check_location_permission = () => {
    this.setState({ location_permission_loading: true }, async () => {
      if (Platform.OS == "android") {
        const granted = (await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)) == PermissionsAndroid.RESULTS.GRANTED;
        this.setState({ location_permission_loading: false, location_permission_granted: granted }, () => {
          if (granted) {
            this.get_current_position();
          }
        });
      } else {
        this.setState({ location_permission_loading: false, location_permission_granted: true }, () => this.get_current_position());
      }
    });
  };

  get_current_position = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        this.setState({ current_location: { latitude, longitude }, current_position_loading: false });
      },
      (error) => {
        // See error code charts below.
        console.log(error.code, error.message);
        SimpleToast.show(error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
    );
  };

  get_data = async () => {
    const user_data = await nr_as_get_object("user");
    this.setState({ user: user_data }, () => {
      this.get_room_details();
    });
  };

  register_websocket = () => {
    this.ws = io(WS_URL);

    this.ws.emit("join_room", {
      user_id: this.state.user._id,
      room_id: this.state.component_params.room._id,
    });

    this.ws.on("update_run_room_stats", (data) => {
      this.handle_received_data_from_ws(data);
    });
    this.ws.on("stop_run_in_room", (data) => {
      this.handle_stop_run_in_room(data);
    });
  };

  send_ws_data = () => {
    if (this.ws) {
      this.ws.emit("update_run_room_stats", {
        room_id: this.state.component_params.room._id,
        user: this.state.user,
        stats: { distance: this.state.current_run_distance_in_meters, pace: this.state.current_run_pace_in_minutes_per_km },
      });
    }
  };

  handle_received_data_from_ws = (data) => {
    const { user, stats } = data;
    this.setState({ stats_per_user: { ...this.state.stats_per_user, [user._id]: { user, stats } } });
  };

  handle_stop_run_in_room = () => {
    const { stats_per_user } = this.state;

    let keys = Object.keys(stats_per_user);

    let best_pace = stats_per_user[keys[0]].stats.pace;
    let best_pace_user_id = keys[0];
    let best_distance = stats_per_user[keys[0]].stats.distance;
    let best_distance_user_id = keys[0];

    for (let i = 1, leni = keys.length; i < leni; i++) {
      if (stats_per_user[keys[i]].stats.pace < best_pace) {
        best_pace = stats_per_user[keys[i]].stats.pace;
        best_pace_user_id = keys[i];
      }
      if (stats_per_user[keys[i]].distance > best_distance) {
        best_distance = stats_per_user[keys[i]].stats.distance;
        best_distance_user_id = keys[i];
      }
    }

    msg = `Trening je bil končan.\nNajboljši tempo: ${best_pace}min/KM - Uporabnik: ${stats_per_user[best_pace_user_id].user.username}.\nNajvečja razdalja: ${best_distance} - Uporabnik: ${stats_per_user[best_distance_user_id].user.username}.`;

    SimpleToast.show(msg, SimpleToast.LONG);

    this.setState({ run_has_been_stopped: true, run_has_already_started: false, datetime_run_stop: moment() }, () => {
      const { admin } = this.props.room_details;
      if (admin._id == this.state.user._id) {
        this.props.stop_run_in_room(this.state.component_params.room._id);
      }
    });
  };

  handle_location_change = (c) => {
    if (this.state.run_has_already_started && !this.state.run_has_been_stopped) {
      const { longitude: old_longitude, latitude: old_latitude } = this.state.current_location;
      const { longitude: new_longitude, latitude: new_latitude, speed } = c;

      // calculate distance
      let distance_between_old_loc_and_new_loc = getPreciseDistance(
        { latitude: old_latitude, longitude: old_longitude }, // old location
        { latitude: new_latitude, longitude: new_longitude }, /// new location
      );

      let { current_run_distance_in_meters, current_run_max_speed_in_kmh, current_run_time_elapsed_in_seconds } = this.state;

      // calculate new total distance
      let _current_run_distance_in_meters = Number((current_run_distance_in_meters + distance_between_old_loc_and_new_loc).toFixed(2));
      let _current_run_speed_in_kmh = Number((speed * 3.6).toFixed(2));

      // check if current speed is greater than max speed
      if (_current_run_speed_in_kmh > current_run_max_speed_in_kmh) {
        current_run_max_speed_in_kmh = _current_run_speed_in_kmh;
      }

      let current_run_time_elapsed_in_minutes = current_run_time_elapsed_in_seconds / 60;
      let current_run_distance_in_kilometers = _current_run_distance_in_meters / 1000;
      let _current_run_pace_in_minutes_for_km = Number((current_run_time_elapsed_in_minutes / current_run_distance_in_kilometers).toFixed(2));
      let _current_run_average_speed_in_kmh = Number(((_current_run_distance_in_meters / current_run_time_elapsed_in_seconds) * 3.6).toFixed(2));

      let { current_run_min_altitude_in_meters, current_run_max_altitude_in_meters } = this.state;
      if (c.altitude < current_run_min_altitude_in_meters) current_run_min_altitude_in_meters = c.altitude;
      else if (c.altitde > current_run_max_altitude_in_meters) current_run_max_altitude_in_meters = c.altitude;

      this.setState(
        {
          current_run_distance_in_meters: _current_run_distance_in_meters,
          current_run_speed_in_kmh: _current_run_speed_in_kmh,
          current_run_max_speed_in_kmh,
          current_run_pace_in_minutes_per_km: _current_run_pace_in_minutes_for_km,
          current_run_average_speed_in_kmh: _current_run_average_speed_in_kmh,
          current_run_polyline_coordinates: [...this.state.current_run_polyline_coordinates, { longitude: new_longitude, latitude: new_latitude }],
          current_run_min_altitude_in_meters,
          current_run_max_altitude_in_meters,
          current_run_altitude_in_meters: c.altitude,
          current_location: c,
        },
        () => this.send_ws_data(),
      );
    } else {
      this.setState({ current_location: c });
    }
  };

  get_room_details = () => {
    const { component_params } = this.state;
    this.setState({ expect_room_details_response: true }, () => this.props.get_room_details(component_params.room._id));
  };

  clear_time_interval = () => {
    if (this.time_calculator) {
      clearInterval(this.time_calculator);
    }
  };

  register_time_interval = () => {
    this.time_calculator = setInterval(this.calculate_time, 1000);
  };

  calculate_time = () => {
    const { room } = this.props.room_details;
    const { time_entered_room, run_has_already_started } = this.state;

    const current_datetime = moment();
    const current_run_time_elapsed_in_seconds = current_datetime.diff(time_entered_room, "seconds");

    const diff = moment().diff(moment(room.datetime_run_start), "seconds");
    const diff_formated = moment.duration(diff, "seconds").format("HH:mm:ss", { trim: false });

    this.setState({ time_display: diff_formated, current_run_time_elapsed_in_seconds }, () => {
      if (!run_has_already_started && diff >= 0) {
        this.setState({ run_has_already_started: true }, () => this.register_websocket());
      }
    });
  };

  prepare_room_details = () => {
    const { room, admin, guests } = this.props.room_details;

    const run_has_already_started = moment(room.datetime_run_start).isBefore(moment());

    this.setState({ run_has_already_started, check_if_run_has_already_started_loading: false }, () => {
      this.register_time_interval();
      if (run_has_already_started) {
        this.register_websocket();
      }
    });
  };

  render_time_display = () => {
    const { time_display, run_has_already_started, check_if_run_has_already_started_loading, run_has_been_stopped } = this.state;

    if (check_if_run_has_already_started_loading || run_has_been_stopped) {
      return (
        <Text style={[styles.time_display, {}]}>
          <Text style={styles.time_display}>--:--:--</Text>
        </Text>
      );
    }

    if (run_has_already_started) {
      return (
        <Text style={[styles.time_display, {}]}>
          Trajanje: <Text style={styles.time_display}>{time_display}</Text>
        </Text>
      );
    }

    return (
      <Text style={[styles.time_display, {}]}>
        Do začetka še: <Text style={[styles.time_display, { color: "#FF9F1C" }]}>{time_display}</Text>
      </Text>
    );
  };

  format_distance_from_m_to_km = (m = 0) => {
    return (m / 1000).toFixed(2);
  };

  render_users = () => {
    const { room_details } = this.props;
    const { room, admin, guests } = room_details;
    const { stats_per_user, user, current_run_distance_in_meters, current_run_pace_in_minutes_per_km } = this.state;

    return (
      <View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          {/* current user */}
          <MaterialIcons name={"emoji-people"} color={"#2EC4B6"} size={20} />
          <Text style={{ fontWeight: "bold", color: "#2EC4B6", marginLeft: 5 }}>
            {user.username} - {this.format_distance_from_m_to_km(current_run_distance_in_meters)}km - {current_run_pace_in_minutes_per_km}min/KM
          </Text>
        </View>
        {Object.values(stats_per_user).map((u) => {
          if (u.user._id == user._id) return null;
          return (
            <View key={u.user._id} style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
              {/* current user */}
              <MaterialIcons name={"emoji-people"} color={"#fff"} size={20} />
              <Text style={{ fontWeight: "bold", color: "#fff", marginLeft: 5 }}>
                {u.user.username} - {this.format_distance_from_m_to_km(u.stats.distance)}km - {u.stats.pace}min/KM
              </Text>
            </View>
          );
        })}
      </View>
    );
  };

  render_stop_button = () => {
    return <NRButton title="STOP" color={"red"} onPress={() => this.ws.emit("stop_run_in_room", { room_id: this.state.component_params.room._id })} />;
  };

  render() {
    const { room_details_loading, room_details } = this.props;
    const { current_location, location_permission_granted, location_permission_loading, current_position_loading } = this.state;

    if (room_details_loading || location_permission_loading || current_position_loading) {
      return (
        <View style={styles.spinner_container}>
          <ActivityIndicator />
        </View>
      );
    }

    if (!location_permission_granted) {
      return (
        <View style={styles.spinner_container}>
          <Text>Prosimo, dovolite aplikaciji uporabo lokacijskih storitev.</Text>
        </View>
      );
    }

    const { room, admin, guests } = room_details;
    const { time_display, run_has_already_started, user, run_has_been_stopped, datetime_run_stop } = this.state;

    return (
      <ScrollView contentContainerStyle={styles.other_content_container}>
        <View style={styles.sub_container}>
          <Text style={[styles.time_display, {}]}>
            Soba: <Text style={[styles.time_display, {}]}>{room.name}</Text>
          </Text>
          <Text style={[styles.time_display, {}]}>
            Začetek: <Text style={[styles.time_display, {}]}>{moment(room.datetime_run_start).format("DD.MM.YYYY HH:mm")}</Text>
          </Text>
          {run_has_been_stopped ? (
            <Text style={[styles.time_display, {}]}>
              Konec: <Text style={[styles.time_display, {}]}>{moment(datetime_run_stop).format("DD.MM.YYYY HH:mm")}</Text>
            </Text>
          ) : null}

          {this.render_time_display()}
        </View>

        <View style={styles.sub_container}>{this.render_users()}</View>

        <View style={[styles.map_container, { height: 400, overflow: "hidden" }]}>
          <MapView
            provider={PROVIDER_GOOGLE}
            showsUserLocation={true}
            userLocationPriority={"high"}
            followsUserLocation={true}
            userInterfaceStyle={"dark"}
            region={{
              latitude: current_location.latitude || 37.78825,
              longitude: current_location.longitude || -122.4324,
              latitudeDelta: 0.004,
              longitudeDelta: 0.005,
            }}
            style={styles.map}
            onUserLocationChange={(e) => this.handle_location_change(e.nativeEvent.coordinate)}
          ></MapView>
        </View>

        {admin._id == user._id && !run_has_been_stopped && run_has_already_started ? <View style={styles.sub_container}>{this.render_stop_button()}</View> : null}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  spinner_container: { flex: 1, padding: 20, justifyContent: "center" },
  main_container: { marginTop: 20, marginHorizontal: 20, borderRadius: 10, padding: 20, backgroundColor: "#2e2e2e" },
  sub_container: { marginTop: 20, marginHorizontal: 20, borderRadius: 10, padding: 20, backgroundColor: "#2e2e2e" },
  time_display: { color: "#FF9F1C", fontWeight: "300", fontSize: 16 },
  map_container: {
    marginTop: 20,
    marginHorizontal: 20,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "#1a1a1a",
  },
  other_content_container: { flexGrow: 1 },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

const mapStateToProps = (state) => ({
  room_details_loading: state.run_room.room_details_loading,
  room_details: state.run_room.room_details,
});

const mapDispatchToProps = { get_room_details, stop_run_in_room, get_active_run_rooms };

export default connect(mapStateToProps, mapDispatchToProps)(RunRoomDetails);
