import React, { Component } from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";
import { login_user } from "../actions/AuthActions";
import NRButton from "../components/NRButton";
import NRSwitch from "../components/NRSwitch";
import NRTextInput from "../components/NRTextInput";
import { nr_as_get_object, nr_as_get_string, nr_as_set_object, nr_as_set_string } from "../misc/async_storage";

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      identifier: "",
      password: "",
      secure_password_entry: true,
      remember_me: false,
      auto_login: false,
      expect_login_user_response: false,
      fetch_local_data_loading: true,
    };
  }

  componentDidMount() {
    this.fetch_local_data();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.expect_login_user_response && prevProps.login_loading && !this.props.login_loading) {
    }
  }

  fetch_local_data = () => {
    this.setState({ fetch_local_data_loading: true }, async () => {
      const auto_login = await nr_as_get_object("auto_login");
      const remember_me = await nr_as_get_object("remember_me");
      const identifier = await nr_as_get_string("identifier");
      const password = await nr_as_get_string("password");

      this.setState({ auto_login, remember_me, identifier, password, fetch_local_data_loading: false }, () => {
        if (auto_login) {
          this.handle_login_button_press();
        }
      });
    });
  };

  handle_remember_me_change = (new_value) => {
    this.setState({ remember_me: new_value }, async () => {
      await nr_as_set_object("remember_me", new_value);
    });
  };

  handle_auto_login_change = async (new_value) => {
    this.setState({ auto_login: new_value }, async () => {
      await nr_as_set_object("auto_login", new_value);
    });
  };

  handle_login_button_press = async () => {
    const { identifier, password, remember_me } = this.state;
    if (remember_me) {
      await nr_as_set_string("identifier", identifier);
      await nr_as_set_string("password", password);
    }
    this.setState({ expect_login_user_response: true }, () => {
      this.props.login_user(identifier, password);
    });
  };

  render() {
    const { identifier, password, secure_password_entry, remember_me, auto_login, fetch_local_data_loading } = this.state;
    const { login_loading } = this.props;

    if (fetch_local_data_loading) {
      return (
        <View style={styles.spinner_container}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <View style={styles.main_container}>
        <Text style={styles.heading_text}>PRIJAVA</Text>

        {/* identifier field */}
        <NRTextInput
          label={"Uporabniško ime ali e-naslov"}
          onChangeText={(value) => this.setState({ identifier: value })}
          value={identifier}
          textInputProps={{
            autoCompleteType: "off",
            autoCapitalize: "none",
            autoCorrect: false,
          }}
        />

        {/* password field */}
        <NRTextInput
          label={"Geslo"}
          onChangeText={(value) => this.setState({ password: value })}
          value={password}
          textInputProps={{
            autoCompleteType: "off",
            secureTextEntry: secure_password_entry,
            autoCorrect: false,
            autoCapitalize: "none",
          }}
        />

        {/* secure password entry switch */}
        <NRSwitch
          label={"Prikaži geslo?"}
          labelStyle={{ color: "#FDFFFC" }}
          selected={!secure_password_entry}
          onSelect={() => this.setState({ secure_password_entry: !secure_password_entry })}
        />

        {/* remember me switch */}
        <NRSwitch label={"Zapomni me?"} labelStyle={{ color: "#FDFFFC" }} selected={remember_me} onSelect={() => this.handle_remember_me_change(!remember_me)} />

        {/* auto login switch */}
        <NRSwitch label={"Samodejna prijava?"} labelStyle={{ color: "#FDFFFC" }} selected={auto_login} onSelect={() => this.handle_auto_login_change(!auto_login)} />

        {/* login button */}
        <NRButton onPress={() => this.handle_login_button_press()} title="Prijava" color="#2EC4B6" loading={login_loading} containerStyle={{ marginTop: 20 }} />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  login_loading: state.auth.login_loading,
  login_response: state.auth.login_response,
});

const mapDispatchToProps = { login_user };

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  input: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#ddd",
  },
  main_container: {
    flex: 1,
    padding: 20,
    justifyContent: "center",
  },
  description_text: {
    marginVertical: 20,
    color: "#FDFFFC",
    textAlign: "center",
  },
  heading_text: {
    textAlign: "center",
    color: "#2EC4B6",
    fontWeight: "bold",
    fontSize: 42,
    fontStyle: "italic",
    marginBottom: 20,
  },
  buttons_container: {
    flexDirection: "row",
    justifyContent: "center",
  },
  spinner_container: { flex: 1, padding: 20, justifyContent: "center" },
});
