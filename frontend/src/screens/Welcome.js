import React, { Component } from "react";
import {
  ActivityIndicator,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Modal,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import NRButton from "../components/NRButton";
import { nr_as_get_object } from "../misc/async_storage";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { API_URL } from "@env";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

export class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      auto_login_loading: true,
      apiUrlModalVisible: false,
      apiUrl: API_URL,
    };
  }

  componentDidMount = () => {
    this.check_if_should_auto_login();
  };

  check_if_should_auto_login = async () => {
    let auto_login = await nr_as_get_object("auto_login");
    if (auto_login) {
      this.props.navigation.navigate("Login", { auto_login });
    }
    this.setState({ auto_login_loading: false }, () => {});
  };

  render() {
    const { auto_login_loading, apiUrlModalVisible, apiUrl } = this.state;

    if (auto_login_loading) {
      return (
        <View style={styles.main_container}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <SafeAreaView style={styles.main_container}>
        <Text style={styles.heading_text}>NOVARUNNER</Text>
        <Text style={styles.description_text}>
          NOVARUNNER je mobilna aplikacija, namenjena športnikom, za spremljanje lokacije, povprečne hitrosti, razdalje in ostalih različnih
          podatkov med tekom.
        </Text>
        <View style={styles.buttons_container}>
          <NRButton
            onPress={() => this.props.navigation.navigate("Login")}
            title="Prijava"
            color="#2EC4B6"
            containerStyle={{ marginRight: 5 }}
          />
          <NRButton
            onPress={() => this.props.navigation.navigate("Register")}
            title="Registracija"
            color="#FF9F1C"
            containerStyle={{ marginRight: 5, marginLeft: 2 }}
          />
          <TouchableWithoutFeedback onPress={() => this.setState({ apiUrlModalVisible: true })}>
            <View style={{ justifyContent: "center" }}>
              <MaterialIcons name={"settings"} color={"#2e2e2e"} size={28} />
            </View>
          </TouchableWithoutFeedback>
        </View>

        <Modal
          animationType="fade"
          transparent={true}
          visible={apiUrlModalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            this.setState({ apiUrlModalVisible: false });
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>API URL</Text>
              <TextInput value={apiUrl} onChangeText={(e) => this.setState({ apiUrl: e })} style={styles.textInput} />

              <NRButton
                onPress={() =>
                  this.setState({ apiUrlModalVisible: false }, async () => {
                    axios.defaults.baseURL = this.state.apiUrl;
                    await AsyncStorage.setItem("NOVARUNNER#apiUrl", this.state.apiUrl);
                  })
                }
                title="Shrani"
                color="#2EC4B6"
              />
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    padding: 20,
    justifyContent: "center",
  },
  description_text: {
    marginVertical: 20,
    marginHorizontal: 20,
    color: "#FDFFFC",
    textAlign: "center",
  },
  heading_text: {
    textAlign: "center",
    color: "#E71D36",
    fontWeight: "bold",
    fontSize: 42,
    fontStyle: "italic",
  },
  buttons_container: {
    flexDirection: "row",
    justifyContent: "center",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.75)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "#2A2A2A",
    borderRadius: 4,
    padding: 12,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    color: "#fff",
    fontWeight: "bold",
  },
  textInput: {
    minWidth: 220,
    borderRadius: 4,
    padding: 10,
    backgroundColor: "#414141",
    color: "#fff",
    marginBottom: 12,
  },
});

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
