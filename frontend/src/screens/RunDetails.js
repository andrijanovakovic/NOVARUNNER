import moment from "moment";
import React, { Component } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import MapView, { Marker, Polyline, PROVIDER_GOOGLE } from "react-native-maps";
import { connect } from "react-redux";

export class RunDetails extends Component {
  constructor(props) {
    super(props);

    const { component_params = {} } = props.route.params;

    this.state = {
      component_params,
    };
  }

  format_meters = (meters = 0) => {
    if (meters > 1000) {
      return `${(meters / 1000).toFixed(2)} KM`;
    }
    return `${meters} M`;
  };

  get_start_marker_location = () => {
    const { run } = this.state.component_params;
    const { polyline_coordinates } = run;

    return {
      latitude: polyline_coordinates[0].latitude || 37.78825,
      longitude: polyline_coordinates[0].longitude || -122.4324,
    };
  };

  get_end_marker_location = () => {
    const { run } = this.state.component_params;
    const { polyline_coordinates } = run;

    return {
      latitude: polyline_coordinates[polyline_coordinates.length - 1].latitude || 37.78825,
      longitude: polyline_coordinates[polyline_coordinates.length - 1].longitude || -122.4324,
    };
  };

  get_region = () => {
    const { run } = this.state.component_params;
    const { polyline_coordinates } = run;

    return {
      latitude: polyline_coordinates[0].latitude || 37.78825,
      longitude: polyline_coordinates[0].longitude || -122.4324,
      latitudeDelta: 0.004,
      longitudeDelta: 0.005,
    };
  };

  render() {
    const { run } = this.state.component_params;
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={styles.data_container}>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Razdalja</Text>
            <Text style={[styles.row_value_text, { color: "#f77f00" }]}>{this.format_meters(run.distance_in_meters)}</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Povprečna hitrost</Text>
            <Text style={[styles.row_value_text, { color: "#d62828" }]}>{run.average_speed_in_kmh} KM/H</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Največja hitrost</Text>
            <Text style={[styles.row_value_text, { color: "#fcbf49" }]}>{run.max_speed_in_kmh} KM/H</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Trajanje</Text>
            <Text style={[styles.row_value_text, { color: "#90e0ef" }]}>{run.duration_formatted_as_hh_mm_ss}</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Tempo</Text>
            <Text style={[styles.row_value_text, { color: "#f72585" }]}>{run.pace_in_minutes_per_km} Min/KM</Text>
          </View>
        </View>
        <View style={styles.data_container}>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Najvišja nadmorska višina</Text>
            <Text style={[styles.row_value_text, { color: "#caffbf" }]}>{run.max_altitude_in_meters} M</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Najnižja nadmorska višina</Text>
            <Text style={[styles.row_value_text, { color: "#bdb2ff" }]}>{run.min_altitude_in_meters} M</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Višinska sprememba</Text>
            <Text style={[styles.row_value_text, { color: "#bdb2ff" }]}>{run.overall_altitude_change_in_meters} M</Text>
          </View>
        </View>
        <View style={styles.data_container}>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Začetek</Text>
            <Text style={[styles.row_value_text, { color: "#caffbf", flex: 3, fontSize: 24 }]}>{moment(run.datetime_started).format("DD.MM.YYYY HH:mm:ss")}</Text>
          </View>
          <View style={styles.row_container}>
            <Text style={styles.row_label_text}>Konec</Text>
            <Text style={[styles.row_value_text, { color: "#caffbf", flex: 3, fontSize: 24 }]}>{moment(run.datetime_ended).format("DD.MM.YYYY HH:mm:ss")}</Text>
          </View>
        </View>
        {run.polyline_coordinates && run.polyline_coordinates.length != 0 ? (
          <View style={[styles.data_container, { flex: 1, height: 500, marginBottom: 20, overflow: "hidden" }]}>
            <MapView provider={PROVIDER_GOOGLE} userInterfaceStyle={"dark"} region={this.get_region()} style={styles.map}>
              <Polyline coordinates={run.polyline_coordinates} strokeColor="#f77f00" strokeWidth={6} />
              <Marker coordinate={this.get_start_marker_location()} pinColor={"#006466"}></Marker>
              <Marker coordinate={this.get_end_marker_location()} pinColor={"#d62828"}></Marker>
            </MapView>
          </View>
        ) : null}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  data_container: {
    marginTop: 20,
    marginHorizontal: 20,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "#1a1a1a",
  },
  row_container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  row_label_text: {
    flex: 1.5,
    color: "#fdfffc",
  },
  row_value_text: {
    textAlign: "right",
    flex: 2,
    fontSize: 32,
    fontWeight: "100",
    color: "#fdfffc",
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    borderRadius: 10,
  },
});

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(RunDetails);
