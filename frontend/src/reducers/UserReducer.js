export const GET_USER_INFO_LOADING = "GET_USER_INFO_LOADING";
export const GET_USER_INFO_SUCCESS = "GET_USER_INFO_SUCCESS";
export const GET_USER_INFO_FAIL = "GET_USER_INFO_FAIL";

export const SAVE_USER_INFO_LOADING = "SAVE_USER_INFO_LOADING";
export const SAVE_USER_INFO_SUCCESS = "SAVE_USER_INFO_SUCCESS";
export const SAVE_USER_INFO_FAIL = "SAVE_USER_INFO_FAIL";

export const initial_state = {
  user_info_loading: false,
  user_info: [],
  save_user_info_loading: false,
  save_user_info_response: [],
};

export default (state = initial_state, action) => {
  switch (action.type) {
    case GET_USER_INFO_LOADING:
      return { ...state, user_info_loading: true };
    case GET_USER_INFO_SUCCESS:
      return { ...state, user_info_loading: false, user_info: action.payload };
    case GET_USER_INFO_FAIL:
      return { ...state, user_info_loading: false };

    case SAVE_USER_INFO_LOADING:
      return { ...state, save_user_info_loading: true };
    case SAVE_USER_INFO_SUCCESS:
      return { ...state, save_user_info_loading: false, save_user_info_response: action.payload };
    case SAVE_USER_INFO_FAIL:
      return { ...state, save_user_info_loading: false };

    // default actions
    case "reset":
      return init(action.payload);
    default:
      return state;
  }
};
