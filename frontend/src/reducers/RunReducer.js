import moment from "moment";

export const set_new_run_datetime_started = "set_new_run_datetime_started";
export const set_new_run_target_duration_in_seconds = "set_new_run_target_duration_in_seconds";
export const set_new_run_target_distance_in_meters = "set_new_run_target_distance_in_meters";

export const GET_LAST_TRAINING_LOADING = "GET_LAST_TRAINING_LOADING";
export const GET_LAST_TRAINING_SUCCESS = "GET_LAST_TRAINING_SUCCESS";
export const GET_LAST_TRAINING_FAIL = "GET_LAST_TRAINING_FAIL";

export const START_TRAINING_LOADING = "START_TRAINING_LOADING";
export const START_TRAINING_SUCCESS = "START_TRAINING_SUCCESS";
export const START_TRAINING_FAIL = "START_TRAINING_FAIL";

export const CHECK_FOR_UNFINISHED_TRAINING_LOADING = "CHECK_FOR_UNFINISHED_TRAINING_LOADING";
export const CHECK_FOR_UNFINISHED_TRAINING_SUCCESS = "CHECK_FOR_UNFINISHED_TRAINING_SUCCESS";
export const CHECK_FOR_UNFINISHED_TRAINING_FAIL = "CHECK_FOR_UNFINISHED_TRAINING_FAIL";

export const STOP_TRAINING_LOADING = "STOP_TRAINING_LOADING";
export const STOP_TRAINING_SUCCESS = "STOP_TRAINING_SUCCESS";
export const STOP_TRAINING_FAIL = "STOP_TRAINING_FAIL";

export const GET_PREVIOUS_RUNS_LOADING = "GET_PREVIOUS_RUNS_LOADING";
export const GET_PREVIOUS_RUNS_SUCCESS = "GET_PREVIOUS_RUNS_SUCCESS";
export const GET_PREVIOUS_RUNS_FAIL = "GET_PREVIOUS_RUNS_FAIL";

export const run_reducer_init_state = {
  new_run_datetime_started: moment().format("DD.MM.YYYY HH:mm:ss"),
  new_run_target_duration_in_seconds: null,
  new_run_target_distance_in_meters: null,
  get_last_training_loading: false,
  last_training_data: [],
  start_training_loading: false,
  start_training_response: [],
  check_for_unfinished_training_loading: false,
  check_for_unfinished_training_response: [],
  stop_training_loading: false,
  stop_training_response: [],
  previous_runs_loading: false,
  previous_runs: [],
  previous_runs_stats: [],
};

export default (state = run_reducer_init_state, action) => {
  switch (action.type) {
    case set_new_run_datetime_started:
      return { ...state, new_run_datetime_started: action.payload };
    case set_new_run_target_duration_in_seconds:
      return { ...state, new_run_target_duration_in_seconds: action.payload };
    case set_new_run_target_distance_in_meters:
      return { ...state, new_run_target_distance_in_meters: action.payload };

    case GET_LAST_TRAINING_LOADING:
      return { ...state, get_last_training_loading: true };
    case GET_LAST_TRAINING_SUCCESS:
      return { ...state, get_last_training_loading: false, last_training_data: action.payload };
    case GET_LAST_TRAINING_FAIL:
      return { ...state, get_last_training_loading: false };

    case START_TRAINING_LOADING:
      return { ...state, start_training_loading: true };
    case START_TRAINING_SUCCESS:
      return { ...state, start_training_loading: false, start_training_response: action.payload };
    case START_TRAINING_FAIL:
      return { ...state, start_training_loading: false };

    case CHECK_FOR_UNFINISHED_TRAINING_LOADING:
      return { ...state, check_for_unfinished_training_loading: true };
    case CHECK_FOR_UNFINISHED_TRAINING_SUCCESS:
      return { ...state, check_for_unfinished_training_loading: false, check_for_unfinished_training_response: action.payload };
    case CHECK_FOR_UNFINISHED_TRAINING_FAIL:
      return { ...state, check_for_unfinished_training_loading: false };

    case STOP_TRAINING_LOADING:
      return { ...state, stop_training_loading: true };
    case STOP_TRAINING_SUCCESS:
      return { ...state, stop_training_loading: false, stop_training_response: action.payload };
    case STOP_TRAINING_FAIL:
      return { ...state, stop_training_loading: false };

    case GET_PREVIOUS_RUNS_LOADING:
      return { ...state, previous_runs_loading: true };
    case GET_PREVIOUS_RUNS_SUCCESS:
      return { ...state, previous_runs_loading: false, previous_runs: action.payload, previous_runs_stats: action.stats };
    case GET_PREVIOUS_RUNS_FAIL:
      return { ...state, previous_runs_loading: false };

    // default actions
    case "reset":
      return init(action.payload);
    default:
      return state;
  }
};
