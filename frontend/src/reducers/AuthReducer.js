export const register_user_loading = "register_user_loading";
export const register_user_success = "register_user_success";
export const register_user_fail = "register_user_fail";
export const login_user_loading = "login_user_loading";
export const login_user_success = "login_user_success";
export const login_user_fail = "login_user_fail";
export const LOGOUT_USER = "LOGOUT_USER";

export const initial_state = {
  register_user_loading: false,
  register_user_response: [],
  login_loading: false,
  login_response: [],
  is_auth: false,
};

export default (state = initial_state, action) => {
  switch (action.type) {
    case register_user_loading:
      return { ...state, register_user_loading: true };
    case register_user_success:
      return { ...state, register_user_loading: false, register_user_response: action.payload };
    case register_user_fail:
      return { ...state, register_user_loading: false };

    case login_user_loading:
      return { ...state, login_loading: true };
    case login_user_success:
      return { ...state, login_loading: false, login_response: action.payload, is_auth: true };
    case login_user_fail:
      return { ...state, login_loading: false };

    case LOGOUT_USER:
      return { ...state, is_auth: false };

    // default actions
    case "reset":
      return init(action.payload);
    default:
      return state;
  }
};
