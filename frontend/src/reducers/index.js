import { combineReducers } from "redux";

// network reducer
import { reducer as network_reducer } from "react-native-offline";
import AuthReducer from "./AuthReducer";
import RunReducer from "./RunReducer";
import UserReducer from "./UserReducer";
import RunRoomReducer from "./RunRoomReducer";

// reducers

const appReducer = combineReducers({
  auth: AuthReducer,
  run: RunReducer,
  network: network_reducer,
  user: UserReducer,
  run_room: RunRoomReducer,
});

const rootReducer = (state, action) => {
  // when a logout action is dispatched it will reset redux state
  if (action.type === "RESET_REDUCERS") {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
