export const GET_ACTIVE_RUN_ROOMS_LOADING = "GET_ACTIVE_RUN_ROOMS_LOADING";
export const GET_ACTIVE_RUN_ROOMS_SUCCESS = "GET_ACTIVE_RUN_ROOMS_SUCCESS";
export const GET_ACTIVE_RUN_ROOMS_FAIL = "GET_ACTIVE_RUN_ROOMS_FAIL";

export const GET_ROOM_DETAILS_LOADING = "GET_ROOM_DETAILS_LOADING";
export const GET_ROOM_DETAILS_SUCCESS = "GET_ROOM_DETAILS_SUCCESS";
export const GET_ROOM_DETAILS_FAIL = "GET_ROOM_DETAILS_FAIL";

export const STOP_RUN_IN_ROOM_LOADING = "STOP_RUN_IN_ROOM_LOADING";
export const STOP_RUN_IN_ROOM_SUCCESS = "STOP_RUN_IN_ROOM_SUCCESS";
export const STOP_RUN_IN_ROOM_FAIL = "STOP_RUN_IN_ROOM_FAIL";

export const CREATE_ROOM_LOADING = "CREATE_ROOM_LOADING";
export const CREATE_ROOM_SUCCESS = "CREATE_ROOM_SUCCESS";
export const CREATE_ROOM_FAIL = "CREATE_ROOM_FAIL";

export const run_reducer_init_state = {
  active_run_rooms_loading: true,
  active_run_rooms: [],
  room_details_loading: true,
  room_details: [],
  stop_run_in_room_loading: false,
  stop_run_in_room_response: [],
  create_room_loading: false,
  create_room_response: [],
};

export default (state = run_reducer_init_state, action) => {
  switch (action.type) {
    case GET_ACTIVE_RUN_ROOMS_LOADING:
      return { ...state, active_run_rooms_loading: true };
    case GET_ACTIVE_RUN_ROOMS_SUCCESS:
      return { ...state, active_run_rooms_loading: false, active_run_rooms: action.payload };
    case GET_ACTIVE_RUN_ROOMS_FAIL:
      return { ...state, active_run_rooms_loading: false };

    case GET_ROOM_DETAILS_LOADING:
      return { ...state, room_details_loading: true };
    case GET_ROOM_DETAILS_SUCCESS:
      return { ...state, room_details_loading: false, room_details: action.payload };
    case GET_ROOM_DETAILS_FAIL:
      return { ...state, room_details_loading: false };

    case STOP_RUN_IN_ROOM_LOADING:
      return { ...state, stop_run_in_room_loading: true };
    case STOP_RUN_IN_ROOM_SUCCESS:
      return { ...state, stop_run_in_room_loading: false, stop_run_in_room_response: action.payload };
    case STOP_RUN_IN_ROOM_FAIL:
      return { ...state, stop_run_in_room_loading: false };

    case CREATE_ROOM_LOADING:
      return { ...state, create_room_loading: true };
    case CREATE_ROOM_SUCCESS:
      return { ...state, create_room_loading: false, create_room_response: action.payload };
    case CREATE_ROOM_FAIL:
      return { ...state, create_room_loading: false };

    // default actions
    case "reset":
      return init(action.payload);
    default:
      return state;
  }
};
